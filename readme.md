# Discasst

###### Developed by Sorokin Andrey (October 2018 - April 2019)

### Backend:

**Tech-stack**: Java, SpringBoot, Thymeleaf, Hibernate, Lombok, H2  
**Path**: src/main/java

### Frontend:

**Tech-stack**: React, Webpack, JavaScript, Bootstrap, HTML, CSS  
**Path**: src/main/resources/static

## Описание проекта:

**Discasst** - это платформа, сочетающая в себе возможности работы с подкастами, такие как поиск, рейтинги, рекомендации
и подписки, а также предоставляет возможность обсуждения подкастов с ведущими и другими слушателями.

## Project Description:

**Discasst** is a platform that combines podcast capabilities such as search, ratings, recommendations, and
subscriptions, as well as the ability to discuss podcasts with hosts and other listeners.

![img.png](readme/img.png)

![img_1.png](readme/img_1.png)

![img_2.png](readme/img_2.png)

![img_3.png](readme/img_3.png)

![img_4.png](readme/img_4.png)

![img_5.png](readme/img_5.png)

![img_6.png](readme/img_6.png)

![img_7.png](readme/img_7.png)

![img_8.png](readme/img_8.png)
