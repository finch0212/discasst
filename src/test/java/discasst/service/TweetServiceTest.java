package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TweetServiceTest {

    @Test
    public void create() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(49);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getById() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getAllTwitsByUserId() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getAllTwitsByUserIdFeed() {
        Assert.assertEquals(1, 1);
    }
}