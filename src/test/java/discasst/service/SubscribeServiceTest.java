package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class SubscribeServiceTest {

    @Test
    public void subscribe() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(42);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getMyVote() {
        Assert.assertEquals(1, 1);
    }
}