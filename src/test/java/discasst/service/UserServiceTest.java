package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class UserServiceTest {

    @Test
    public void loadUserByUsername() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(104);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getUserInfo() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void addUser() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void updateProfile() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getUsersCountToday() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getUsersCountThisMounth() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getCoauthorsByIdList() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void loginCheck() {
        Assert.assertEquals(1, 1);
    }
}