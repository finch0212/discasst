package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class RatingServiceTest {

    @Test
    public void vote() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(44);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getMyVote() {
        Assert.assertEquals(1, 1);
    }
}