package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class PodcastServiceTest {

    @Test
    public void getGenres() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(168);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void createPodcast() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void removePodcast() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getPodcastsByUserId() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getPodcastsByUserIdInCoauthors() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void createEpisode() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getAllEpisodesByPodcastIdInPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getEpisodesInFeedByUserIdInPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getEpisodesCountToday() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getEpisodesCountThisMounth() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getPodcastsCountToday() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getPodcastsCountThisMounth() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void addCoauthors() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getCoauthors() {
        Assert.assertEquals(1, 1);
    }
}