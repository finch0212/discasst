package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class SearchServiceTest {

    @Test
    public void getPodcastsInPage() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(62);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getPodcastById() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void getAllGenres() {
        Assert.assertEquals(1, 1);
    }
}