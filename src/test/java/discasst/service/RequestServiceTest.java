package discasst.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class RequestServiceTest {

    @Test
    public void signinPage() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(134);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void registerPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void searchPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void feed() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void statisticsPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void mainPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void userProfilePage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void podcastPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void createNewPodcastPage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void createNewEpisodePage() {
        Assert.assertEquals(1, 1);
    }

    @Test
    public void addCoauthorsPage() {
        Assert.assertEquals(1, 1);
    }
}