export function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

export function getFullCSRFParam() {
    const token = getCookie('XSRF-TOKEN');
    return `_csrf=${token}`;
}

export function saveCookie(cookieName, value) {
    document.cookie = `${cookieName}=${value}; path=/`;
}

export function deleteCookie(cookieName) {
    document.cookie = `${cookieName}=${""}; path=/; expires=-1`;
}

export function saveAllCookie(username, id, email, creationDate, name, bio) {
    saveCookie("username", username);
    saveCookie("userId", id);
    saveCookie("userEmail", email);
    saveCookie("userCreationDate", creationDate);
    saveCookie("name", name);
    saveCookie("bio", bio);
}

export function deleteAllCookie() {
    deleteCookie("username");
    deleteCookie("userId");
    deleteCookie("userEmail");
    deleteCookie("userCreationDate");
    deleteCookie("name");
    deleteCookie("bio");
    deleteCookie("XSRF-TOKEN");
}

export function isUserSignedIn() {
    let username = getCookie("username");
    return (username !== "" && username !== undefined);
}

export const getGenres = () =>
    fetch("/getAllGenres").then(response => response.json());

