import React from 'react';
import EpisodePanel from './EpisodePanel';
import WelcomePanel from './tweets/WelcomePanel';
import {getCookie} from '../Helper';

export default class PodcastList extends React.Component {
    constructor() {
        super();
        this.state = {
            episodes: [],
            pageSize: 5,
            loading: false,
            currPage: 0,
            allPages: false,
        };
    }

    componentDidMount() {
        this.getEpisodes(0);

        const that = this;
        window.onscroll = () => {
            if (!that.state.allPages && !that.state.loading) {
                //(document.body.scrollHeight - window.scrollY - document.body.offsetHeight) <= 100
                if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100)) {
                    let nextPage = that.state.currPage + 1;
                    console.log("End of page, load next page: " + nextPage);
                    that.setState({currPage: nextPage, loading: true});
                    that.getEpisodes(nextPage);
                }
            }
        };
    }

    getEpisodes(page) {
        $.ajax({
            url: `${this.props.feed ? "/getEpisodesInFeedByUserIdInPage" : "/getEpisodesByPodcastIdInPage"}?id=${this.props.feed ? getCookie('userId') : this.props.podcastId}&page=${page}&size=${this.state.pageSize}`,
            type: 'get',
            contentType: 'application/json',
            success: (response) => {
                this.setState(
                    {
                        allPages: response.last,
                        loading: false,
                        episodes: this.state.episodes.concat(response.content)
                    }
                );
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    closeAllComments = () => {
        this.state.episodes.map((ep) => {
            this.refs['episode' + ep.id].closeComments();
        });
    };

    render() {
        this.episodes = [];
        let episodeList = this.state.episodes.map(ep =>
            <EpisodePanel
                episode={ep}
                key={ep.id}
                history={this.props.history}
                closeAllComments={this.closeAllComments}
                ref={'episode' + ep.id}
            />
        );

        return (
            <div>
                {this.state.episodes.length === 0 && <WelcomePanel/>}
                {episodeList}
            </div>
        )
    }
}
