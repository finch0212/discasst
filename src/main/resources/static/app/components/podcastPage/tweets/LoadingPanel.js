const React = require('react');

export default class LoadingPanel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div hidden={this.props.hidden}>
                <img className="center-block" src="/images/loading.svg" width="70" height="70"/>
                <br/>
            </div>
        )
    }
}