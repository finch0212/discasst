const React = require('react');
import Tweet from './Tweet';

export default class TweetList extends React.Component {
    constructor(props) {
        super(props);
    }

    deleteTwitCallback(id) {
        this.props.callbackFromChild(id);
    }

    render() {
        let tweets = this.props.tweets.map(tweet =>
            <Tweet id={"tweet" + tweet.id}
                   key={"tweet" + tweet.id}
                   tweet={tweet}
                   callbackFromChild={this.deleteTwitCallback.bind(this)}
                   myPodcast={this.props.myPodcast}
                   history={this.props.history}
            />
        );
        const date = new Date();
        console.log("Rerender TweetList at time: " + date.getMinutes() + "." + date.getSeconds() + "." + date.getMilliseconds());
        return (
            <div>
                {tweets}
            </div>
        )
    }
}