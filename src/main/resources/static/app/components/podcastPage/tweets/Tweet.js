const React = require('react');
const url = '/deleteTweet?tweetId=';
import DeleteTweetButton from './DeleteTweetButton';
import {getCookie, getFullCSRFParam} from "../../Helper";

export default class Tweet extends React.Component {
    constructor(props) {
        super(props);
    }

    delete() {
        const that = this;
        const tweetId = this.props.tweet.id;
        $.ajax({
            url: url + tweetId + '&' + getFullCSRFParam(),
            type: 'post',
            success: () => {
                that.props.callbackFromChild(tweetId);
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    render() {
        const {text, creationDate, authorUsername} = this.props.tweet;
        return (
            <div className="container-fluid">
                <div className="panel panel-info" style={{marginBottom: 10}}>
                    <div className="panel-body" style={{backgroundColor: '#ffffff'}}>
                        <div>
                            {this.props.myPodcast && getCookie("username") === authorUsername &&
                            <DeleteTweetButton onDelete={this.delete.bind(this)}/>}
                            <font size="3"><a onClick={() => {
                                this.props.history.push(`/user/${authorUsername}`)
                            }}>@{authorUsername}</a></font>
                            <h5 style={{color: '#acacac', marginBottom: 0}}>{creationDate}</h5>
                        </div>
                        <hr style={{margin: "10px 0 15px 0"}}/>
                        <font size="3" style={{whiteSpace: "pre-wrap"}}>{text}</font>
                    </div>
                </div>
            </div>
        )
    }
}
