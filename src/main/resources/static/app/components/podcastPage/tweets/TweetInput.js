import {getCookie, getFullCSRFParam} from "../../Helper";

const React = require('react');
const addTweetUrl = '/addTweet';

export default class TweetInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
    }

    handleSubmit = () => {
        if (this.state.value !== '') {
            this.addNewTweet();
        }
    };

    addNewTweet() {
        let obj = {
            "text": this.state.value,
            "authorUsername": getCookie("username"),
            "authorId": getCookie("userId"),
            "podcastId": this.props.podcastId
        };
        let tweetJson = JSON.stringify(obj);
        const that = this;
        $.ajax({
            url: addTweetUrl + '?' + getFullCSRFParam(),
            type: 'post',
            contentType: 'application/json',
            data: tweetJson,
            success: () => {
                that.afterPostNewTwit();
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    afterPostNewTwit() {
        console.log("Tweet: " + this.state.value);
        this.setState({value: ''});
        this.refs.tweetTextarea.value = '';
        this.refs.tweetTextarea.focus();
        this.props.callbackFromChild();
    }

    handleChange = (event) => {
        this.setState({value: event.target.value});
    };

    render() {
        return (
            <div className="container-fluid">
                <div className="panel panel-info">
                    <div className="panel-body">
                        <div className="form-group shadow-textarea">
                            <textarea style={{resize: 'none'}} className="form-control z-depth-1"
                                      rows="4" maxLength="200" autoFocus ref="tweetTextarea"
                                      placeholder="What's happening?" onChange={this.handleChange}>
                            </textarea>
                        </div>
                        <div className="btn-group-md">
                            <button onClick={this.handleSubmit} type="button" className="btn btn-info pull-right"
                                    style={{width: 100}}>Tweet
                        </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}