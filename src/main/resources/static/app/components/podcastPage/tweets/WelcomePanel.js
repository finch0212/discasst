const React = require('react');

export default class WelcomePanel extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="panel panel-info" style={{height: '240px'}}>
                    <div className="panel-body text-center">
                        <h3 style={{color: '#a0a0a0'}}>Здесь еще ничего нет,</h3>
                        <h3 style={{color: '#a0a0a0'}}>но скоро обязательно что-то появится!</h3>
                        <br/>
                        <img src="/images/smile.png" width="60"/>
                    </div>
                </div>
            </div>
        )
    }
}