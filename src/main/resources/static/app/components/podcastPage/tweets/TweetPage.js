const React = require('react');
import TweetList from './TweetList';
import TweetInput from './TweetInput';
import WelcomePanel from './WelcomePanel';
import LoadingPanel from './LoadingPanel';
import {getCookie} from '../../Helper';

export default class TweetPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            currPage: 0,
            pageSize: 10,
            allPages: false,
            numberOfTweets: 0,
            loading: false,
            loadImg: null
        };
    }

    componentDidMount() {
        this.getNewPageOfTweetsFromServer(0, this.state.pageSize * (this.state.currPage + 1));
        const that = this;
        /**
         * Проверяет конец страницы, если так и страница не последняя, то грузит новую страницу твитов.
         */
        window.onscroll = () => {
            if (!that.state.allPages && !that.state.loading) {
                if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100)) {
                    let nextPage = that.state.currPage + 1;
                    console.log("End of page, load next page: " + nextPage);
                    that.setState({currPage: nextPage, loading: true});
                    that.getNewPageOfTweetsFromServer(nextPage, that.state.pageSize);
                }
            }
        };
    }

    /**
     * Грузит с сервера последний твит, помещает его в массив твитов, удаляет последний элемент массива
     * если не последняя страница и ререндерит компонент.
     */
    refreshTweetArrayAfterAdd() {
        fetch(`${this.props.feed ? "/getAllTwitsByUserIdFeed" : "/getAllTwitsByPodcastId"}?page=0&size=1&id=${this.props.feed ? getCookie('userId') : this.props.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                let array = this.state.tweets;
                if (!this.state.allPages) array.splice(array.length - 1, 1);
                this.setState({tweets: json.content.concat(array)});
            });
    }

    /**
     * Загружает заново с сервера все страницы и ререндерит компонент.
     * @param id айдишник удаляемого твита.
     */
    refreshTweetArrayAfterDelete(id) {
        fetch(`${this.props.feed ? "/getAllTwitsByUserIdFeed" : "/getAllTwitsByPodcastId"}?page=0&size=${(this.state.currPage + 1) * this.state.pageSize}&id=${this.props.feed ? getCookie('userId') : this.props.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                this.setState({tweets: json.content});
                const date = new Date();
                console.log("Deleting tweet at time: " + date.getMinutes() + "." + date.getSeconds() + "." + date.getMilliseconds());
                this.lastPageCheck(json);
            });
    }

    /**
     * Получает новую страницу твитов, добавляет её к уже существующему массиву твитов и ререндерит компонент.
     */
    getNewPageOfTweetsFromServer(page, pagesize) {
        fetch(`${this.props.feed ? "/getAllTwitsByUserIdFeed" : "/getAllTwitsByPodcastId"}?page=${page}&size=${pagesize}&id=${this.props.feed ? getCookie('userId') : this.props.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                this.setState({tweets: this.state.tweets.concat(json.content)});
                this.setState({numberOfTweets: json.totalElements});
                this.lastPageCheck(json);
                this.setState({loading: false});
            });
    }

    lastPageCheck(json) {
        this.setState({allPages: json.last});
    }

    addTweetCallback() {
        this.refreshTweetArrayAfterAdd();
        this.setState({numberOfTweets: (this.state.numberOfTweets + 1)});
    }

    deleteTwitCallback(id) {
        this.refreshTweetArrayAfterDelete(id);
        this.setState({numberOfTweets: (this.state.numberOfTweets - 1)});
    }

    render() {
        return (
            <div>
                {this.props.myPodcast &&
                <TweetInput podcastId={this.props.podcastId} callbackFromChild={this.addTweetCallback.bind(this)}/>}
                <TweetList
                    tweets={this.state.tweets}
                    callbackFromChild={this.deleteTwitCallback.bind(this)}
                    myPodcast={this.props.myPodcast}
                    history={this.props.history}
                />
                {!this.state.loading && this.state.numberOfTweets === 0 && <WelcomePanel/>}
                <LoadingPanel hidden={!this.state.loading}/>
            </div>
        )
    }
}