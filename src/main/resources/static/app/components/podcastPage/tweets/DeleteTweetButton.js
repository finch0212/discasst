const React = require('react');

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button type="button"
                    className="btn btn-info btn-xs pull-right"
                    onClick={this.props.onDelete}
                    style={{marginLeft: -24}}
            >
                <span className="glyphicon glyphicon-remove"></span>
            </button>
        )
    }
}