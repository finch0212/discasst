import React from 'react';
import ReactDisqusComments from 'react-disqus-comments';

export default class EpisodePanel extends React.Component {
    constructor() {
        super();
        this.state = {
            isCommentsOn: false
        }
    }

    closeComments = () => {
        this.setState({isCommentsOn: false});
    };

    render() {
        return (
            <div className="container-fluid">
                <div className="panel panel-info" style={{marginBottom: 10}}>
                    <div className="panel-body" style={{maxWidth: '100%'}}>
                        <h4 style={{marginTop: 0}}>{this.props.episode.name}</h4>
                        <h5 style={{color: '#acacac', marginBottom: 0}}>{this.props.episode.date}</h5>
                        <hr style={{marginTop: 10}}/>
                        <iframe width="100%" height="63" src={`${this.props.episode.podsterLink}/embed/15?link=0&ap=0`}
                                frameborder="0" allowtransparency="true"></iframe>
                        <p className="text-justify">{this.props.episode.description}
                        </p>
                        <button
                            onClick={() => {
                                if (!this.state.isCommentsOn) this.props.closeAllComments();
                                this.setState({isCommentsOn: !this.state.isCommentsOn});
                            }}
                            type="button"
                            className="btn btn-info btn-md pull-right "
                        >
                            {this.state.isCommentsOn ? 'Свернуть комментарии' : 'Обсудить'}
                        </button>
                        {this.state.isCommentsOn &&
                        <ReactDisqusComments
                            shortname={'discasst'}
                            identifier={'http://discasst.com/#!/episode/' + this.props.episode.id}
                            title={this.props.episode.name}
                            url={'http://discasst.com/#!/episode/' + this.props.episode.id}
                            onNewComment={() => {
                            }}
                            id={this.props.episode.id}
                        />
                        }
                    </div>
                </div>
            </div>
        );
    }
}