import React, {Component} from 'react';
import {Button, ButtonGroup} from 'react-bootstrap';
import PodcastList from './PodcastList';
import TweetPage from './tweets/TweetPage';

export default class PodcastPageContent extends Component {
    constructor() {
        super();
        this.state = {
            tweetPage: false
        }
    }

    onTweetPageClick = (e) => {
        this.setState({tweetPage: true});
        e.target.blur();
    };

    onPodcastPageClick = (e) => {
        this.setState({tweetPage: false});
        e.target.blur();
    };

    render() {
        return (
            <div style={{textAlign: "center"}}>
                <ButtonGroup style={{marginBottom: 20}}>
                    <Button
                        onClick={this.onPodcastPageClick}
                        style={{width: 100}}
                        bsStyle={!this.state.tweetPage ? "info" : "default"}>
                        Выпуски
                    </Button>
                    <Button
                        onClick={this.onTweetPageClick}
                        style={{width: 100}}
                        bsStyle={this.state.tweetPage ? "info" : "default"}>
                        Микроблог
                    </Button>
                </ButtonGroup>
                {this.state.tweetPage ? <TweetPage
                        feed={this.props.feed}
                        podcastId={this.props.podcastId}
                        myPodcast={this.props.myPodcast}
                        history={this.props.history}
                    /> :
                    <PodcastList
                        feed={this.props.feed}
                        podcastId={this.props.podcastId}
                        history={this.props.history}
                    />
                }
            </div>
        );
    }
}
