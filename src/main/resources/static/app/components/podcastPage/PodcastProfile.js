import {getCookie, getFullCSRFParam} from '../Helper';

const React = require('react');

export default class PodcastProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            genres: null,
            podcast: null,
            myDecision: 0,
            mySubscribe: false,
            isUserSignedIn: false,
            extraSub: 0,
            emptyLinks: false
        };
    }

    componentDidMount() {
        const podcast = this.state.podcast === null ? this.props.podcast : this.state.podcast;
        if (podcast.itunesLink === "" && podcast.vkLink === "" && podcast.twitterLink === "" && podcast.facebookLink === "") this.setState({emptyLinks: true})
        this.isUserLogIn();
        this.loadAllGenres();
    }

    loadAllGenres() {
        fetch(`/getAllGenres`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log(json)
                this.setState({genres: json, loadData: this.state.loadData + 1});
            });
    }

    ratingChange = (rating) => {
        if (this.state.isUserSignedIn === false) {
            this.props.history.push('/signin');
        } else {
            let obj = {
                "podcastId": this.props.podcast.id,
                "userId": getCookie('userId'),
                "decision": rating,
            };
            let rate = JSON.stringify(obj);
            const that = this;
            $.ajax({
                url: `/vote?${getFullCSRFParam()}`,
                type: 'post',
                contentType: 'application/json',
                data: rate,
                success: () => {
                    this.setState({myDecision: rating ? 1 : -1});
                    this.loadNewPodcastInfo();
                },
                error: (jqXhr, textStatus, errorThrown) => {
                    console.log(errorThrown + " " + textStatus);
                }
            });
        }
    };

    isUserLogIn() {
        let username = getCookie("username");
        if (username === "" || username === undefined) {
            this.setState({isUserSignedIn: false});
            return false;
        }
        fetch(`/loginCheck?${getFullCSRFParam()}`, {
            method: 'POST'
        }).then((response) => {
            if (response.status === 200 && !response.redirected) {
                this.setState({isUserSignedIn: true});
                this.loadUserRatingAndSubscribeInfo();
                return true;
            } else {
                this.setState({isUserSignedIn: false});
                return false;
            }
        });
    }

    loadNewPodcastInfo() {
        fetch(`/getPodcastById?id=${this.props.podcast.id}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                this.setState({podcast: json});
            });
    }

    loadUserRatingAndSubscribeInfo() {
        fetch(`/getMyVote?userId=${getCookie("userId")}&podcastId=${this.props.podcast.id}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log(json);
                this.setState({myDecision: json});
                this.loadUserSubscribeInfo();
            });
    }

    loadUserSubscribeInfo() {
        fetch(`/getMySubscribe?userId=${getCookie("userId")}&podcastId=${this.props.podcast.id}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log(json);
                this.setState({mySubscribe: json});
            });
    }

    subscribeStateChange = () => {
        if (this.state.isUserSignedIn === false) {
            this.props.history.push('/signin');
        } else {
            fetch(`/subscribe?userId=${getCookie("userId")}&podcastId=${this.props.podcast.id}`)
                .then((response) => {
                    if (response.status === 200) {
                        this.setState({mySubscribe: !this.state.mySubscribe})
                        this.loadNewPodcastInfo();
                    }
                });
        }
    };

    render() {
        const podcast = this.state.podcast === null ? this.props.podcast : this.state.podcast;
        return (
            <div className="container-fluid">
                <div className="panel panel-info">
                    <div className="panel-body" style={{maxWidth: "100%"}}>
                        <div className="row">
                            <div className="col-xs-4" style={{width: "180px", marginBottom: -20}}>
                                <div className="panel panel-info"
                                     style={{width: "150px", textAlign: "center", paddingBottom: 10}}>
                                    <br></br>
                                    <img src="/images/podcastDefaultLogo.png"
                                         className="img-rounded" style={{width: "120px"}}></img><br></br>
                                    <div className="panel-body">
                                        <button onClick={() => {
                                            this.ratingChange(false)
                                        }} type="button"
                                                className={this.state.myDecision === -1 ? "btn btn-danger btn-xs" : "btn btn-info btn-xs"}>
                                            <span
                                                className="glyphicon glyphicon-minus">
                                            </span>
                                        </button>
                                        &#160;<span className="badge">{podcast.rating}</span>&#160;
                                        <button onClick={() => {
                                            this.ratingChange(true)
                                        }} type="button"
                                                className={this.state.myDecision === 1 ? "btn btn-success btn-xs" : "btn btn-info btn-xs"}>
                                        <span
                                            className="glyphicon glyphicon-plus">
                                        </span>
                                        </button>
                                    </div>
                                </div>
                                <div>
                                    <button
                                        type="button"
                                        style={{width: "150px"}}
                                        className={this.state.mySubscribe ? "btn btn-success btn-md" : "btn btn-info btn-md"}
                                        onClick={() => {
                                            this.subscribeStateChange()
                                        }}
                                    >
                                        {this.state.mySubscribe ? "Отписаться" : "Подписаться"}
                                    </button>
                                </div>
                                <br></br>
                            </div>
                            <div className="container-fluid">
                                <h4>{podcast.name}</h4>
                                <h4>
                                    {this.state.genres !== null &&
                                    <small>{this.state.genres[podcast.genreId].name}</small>}
                                </h4>
                                <hr></hr>


                                <p className="text-justify" style={{minHeight: 160}}>{podcast.description}</p>
                                <hr></hr>
                                <div className="row">
                                    <div className="col-xs-6">
                                        <h5 className="text-center">Подписчиков: {podcast.subscribersCount}</h5>
                                    </div>
                                    <div className="col-xs-6">
                                        <h5 className="text-center">Выпусков: {podcast.episodeCount}</h5>
                                    </div>
                                </div>

                                {!this.state.emptyLinks && <div>
                                    <hr></hr>
                                </div>}
                                {!this.state.emptyLinks && <div className="container-fluid text-center">
                                    {podcast.itunesLink !== "" && <a href={podcast.itunesLink}><img
                                        src="/images/itunesLogo.png"
                                        className="img-rounded" style={{width: "30px", margin: "0 10px 0 10px"}}></img></a>}
                                    {podcast.vkLink !== "" && <a href={podcast.vkLink}><img
                                        src="/images/vkLogo.png"
                                        className="img-rounded" style={{width: "30px", margin: "0 10px 0 10px"}}></img></a>}
                                    {podcast.twitterLink !== "" && <a href={podcast.twitterLink}><img
                                        src="/images/twitterLogo.png"
                                        className="img-rounded" style={{width: "30px", margin: "0 10px 0 10px"}}></img></a>}
                                    {podcast.facebookLink !== "" && <a href={podcast.facebookLink}><img
                                        src="/images/facebookLogo.png"
                                        className="img-rounded" style={{width: "30px", margin: "0 10px 0 10px"}}></img>
                                    </a>}
                                </div>}
                                <hr></hr>
                                <div>
                                    <h5>Ведущие:</h5>
                                    <div className="container-fluid text-center">
                                        {this.props.coauthors.map(item =>
                                            <a key={item.id} onClick={() => {
                                                this.props.history.push(`/user/${item.username}`)
                                            }}>
                                                <h4>{item.name} (@{item.username})</h4>
                                            </a>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}