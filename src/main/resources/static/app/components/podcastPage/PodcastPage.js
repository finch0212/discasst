import Navbar from '../Navbar';
import PodcastProfile from "./PodcastProfile";
import PodcastPageContent from './PodcastPageContent';
import {getCookie} from '../Helper';

const React = require('react');

export default class PodcastPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            podcast: null,
            coauthors: [],
            coauthorsInfo: [],
            myPodcast: false
        };
    }

    componentDidMount() {
        this.loadPodcastInfo();
    }

    loadPodcastInfo() {
        fetch(`/getPodcastById?id=${this.props.match.params.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log(json);
                this.setState({podcast: json});
                this.loadCoauthors();
            });
    }

    loadCoauthors() {
        fetch(`/getCoauthors?podcastId=${this.props.match.params.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                let coauthorsIds = [this.state.podcast.authorId.toString(), ...json.map(x => x.userId.toString())];
                console.log(coauthorsIds)
                this.setState({coauthors: coauthorsIds});
                this.loadCoauthorsInfo(coauthorsIds);
                this.myPodcastCheck();
            });
    }

    myPodcastCheck() {
        if (this.state.coauthors.indexOf(getCookie("userId")) !== -1) this.setState({myPodcast: true});
    }

    loadCoauthorsInfo(coauthorsIds) {
        let data = {
            "ids": coauthorsIds
        };
        $.ajax({
            url: `/getCoauthorsByIdList`,
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: (response) => {
                console.log(response);
                this.setState({coauthorsInfo: response});
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div className="container-fluid">
                    {this.state.podcast !== null &&
                    <div style={{display: "flex",}}>
                        <div style={{width: "25%"}}>
                            <PodcastProfile podcast={this.state.podcast} coauthors={this.state.coauthorsInfo}
                                            history={this.props.history}/>
                        </div>
                        <div style={{width: "50%"}}>
                            <PodcastPageContent
                                feed={false}
                                podcastId={this.state.podcast.id}
                                myPodcast={this.state.myPodcast}
                                history={this.props.history}
                            />
                        </div>
                        <div style={{width: "25%"}}>
                        </div>
                    </div>}
                </div>
            </div>
        )
    }
}