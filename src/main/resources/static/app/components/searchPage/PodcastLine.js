const React = require('react');

export default class PodcastLine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            podcast: null
        };
    }

    render() {
        const podcast = this.state.podcast === null ? this.props.podcast : this.state.podcast;
        return (
            <div className="container-fluid">
                <div className="panel panel-info" style={{marginBottom: 10}}>
                    <div className="panel-body">
                        <div className="row">
                            <div className="col-xs-4" style={{width: "180px"}}>
                                <div className="panel panel-info"
                                     style={{width: "150px", textAlign: "center", marginBottom: 0}}>
                                    <br></br>
                                    <img src="images/podcastDefaultLogo.png"
                                         className="img-rounded" style={{width: "120px"}}></img><br></br>
                                    <div className="panel-body">
                                        <span className="badge">{podcast.rating}</span>
                                    </div>
                                </div>
                            </div>

                            <div className="container-fluid">
                                <h4>{podcast.name}
                                    <small> {this.props.genres[podcast.genreId].name}</small>
                                </h4>
                                <hr/>
                                <p className="text-justify">{podcast.description}
                                </p>
                                <a>
                                    <button type="button"
                                            className="btn btn-info btn-md pull-right "
                                            onClick={() => {
                                                this.props.history.push(`/podcast/${podcast.id}`);
                                            }}
                                    >Подробнее
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}