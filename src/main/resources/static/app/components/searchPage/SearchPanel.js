import React, {Component} from 'react';
import {
    Button,
    ButtonGroup,
    FormControl,
    FormGroup,
    Glyphicon,
    InputGroup,
    ListGroup,
    ListGroupItem
} from 'react-bootstrap';

export default class SearchPanel extends Component {
    constructor() {
        super();
        this.state = {
            filterValue: "",
            unselectedGenres: [],
            byRating: false
        }
    }

    componentDidMount() {
        this.setState({filterValue: this.props.filterValue});
    }

    clearFilter = (e) => {
        this.setState({filterValue: ""});
        e.target.blur();
        this.props.onSubmit("", this.state.unselectedGenres, this.state.byRating);
    };

    applyFilter = (e) => {
        e.preventDefault();
        this.props.onSubmit(this.state.filterValue, this.state.unselectedGenres, this.state.byRating);
    };

    handleChange = (e) => {
        this.setState({filterValue: e.target.value});
    };

    unselectGenre = (id) => {
        let tmp = this.state.unselectedGenres;
        let index = tmp.indexOf(id);
        index === -1 ? tmp = [...tmp, id] : tmp.splice(index, 1);
        this.setState({unselectedGenres: tmp});
    };

    searchHeight = 40;

    changeByRatingVal = (val) => {
        this.setState({byRating: val});
        this.props.onSubmit(this.state.filterValue, this.state.unselectedGenres, val);
    };

    render() {
        return (
            <div className="container-fluid">
                <div className="panel panel-info">
                    <div className="panel-body">

                        <form
                            onSubmit={(event) => {
                                this.applyFilter(event)
                            }}
                        >
                            <FormGroup>
                                <InputGroup>

                                    <FormControl
                                        type="text"
                                        placeholder="Search"
                                        value={this.state.filterValue}
                                        id="searchPanelFilter"
                                        onChange={this.handleChange}
                                        style={{height: this.searchHeight}}
                                    />
                                    <InputGroup.Button>
                                        <Button onClick={this.clearFilter}
                                                style={{height: this.searchHeight, minWidth: this.searchHeight}}>
                                            <Glyphicon glyph="remove" style={{fontSize: "1.2em"}}/>
                                        </Button>
                                    </InputGroup.Button>
                                </InputGroup>
                            </FormGroup>
                        </form>

                        <ButtonGroup style={{width: '100%', marginBottom: 15}}>
                            <Button
                                style={{width: '50%'}}
                                className="btn btn-sm"
                                bsStyle={!this.state.byRating ? "info" : "default"}
                                onClick={() => this.changeByRatingVal(false)}>
                                По новизне
                            </Button>
                            <Button
                                style={{width: '50%'}}
                                className="btn btn-sm"
                                bsStyle={this.state.byRating ? "info" : "default"}
                                onClick={() => this.changeByRatingVal(true)}>
                                По рейтингу
                            </Button>
                        </ButtonGroup>

                        <ListGroup className='search-list' style={{marginBottom: 0}}>
                            {this.props.genres && this.props.genres.map((item, pos) =>
                                <ListGroupItem
                                    style={{height: 50, backgroundColor: pos % 2 === 0 ? "#fcfcfc" : "#ffffff"}}
                                    key={item.id} onClick={(e) => {
                                    this.unselectGenre(item.id);
                                    e.target.blur();
                                }}
                                >
                                    <div style={{display: "flex"}}>
                                        <div style={{minWidth: "50px"}}>
                                            <Glyphicon style={{
                                                color: this.state.unselectedGenres.includes(item.id) ? "#d5d5d5" : "#5bc0de",
                                                fontSize: "1.8em"
                                            }} glyph="ok"/>
                                        </div>
                                        <h5>{item.name}</h5>
                                    </div>
                                </ListGroupItem>
                            )}
                        </ListGroup>
                    </div>
                </div>
            </div>
        );
    }
}
