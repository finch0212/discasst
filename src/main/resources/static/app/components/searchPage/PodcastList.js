const React = require('react');
import PodcastLine from "./PodcastLine";

export default class PodcastList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let podcastList = this.props.podcasts.map(podcast =>
            <PodcastLine
                history={this.props.history}
                key={podcast.id}
                podcast={podcast}
                genres={this.props.genres}/>
        );

        return (
            <div>
                {podcastList}
            </div>
        )
    }
}