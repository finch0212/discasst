import PodcastList from "./PodcastList";
import Navbar from '../Navbar';
import SearchPanel from './SearchPanel';
import {getGenres} from "../Helper";

const React = require('react');

export default class SearchPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            podcasts: [],
            genres: [],
            loadData: 0,
            pageSize: 5,
            loading: false,
            currPage: 0,
            allPages: false,
            filter: "",
            notInGenres: [],
            byRating: false
        };
    }

    componentDidMount() {
        const {state} = this.props.location;
        if (state) {
            this.loadPodcasts(state.filterValue, [], false, 0);
        } else {
            this.loadPodcasts("", [], false, 0);
        }
        this.loadAllGenres();

        const that = this;
        window.onscroll = () => {
            if (!that.state.allPages && !that.state.loading) {
                if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 100)) {
                    let nextPage = that.state.currPage + 1;
                    console.log("End of page, load next page: " + nextPage);
                    that.setState({currPage: nextPage, loading: true});
                    that.loadPodcasts(this.state.filter, this.state.notInGenres, this.state.byRating, nextPage);
                }
            }
        };
    }

    loadAllGenres() {
        getGenres().then(genres => {
            this.setState({genres: genres, loadData: this.state.loadData + 1});
        })
    }

    loadPodcasts(filter, notInGenres, byRating, page) {
        let data = {
            "filter": filter,
            "genres": notInGenres.length === 0 ? [-1] : notInGenres.map(i => i - 1),
            "byRating": byRating
        };
        $.ajax({
            url: `/getPodcastsInPage?page=${page}&size=${this.state.pageSize}`,
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: (response) => {
                console.log(response);
                this.setState(
                    page !== 0 ? {
                            podcasts: this.state.podcasts.concat(response.content),
                            loading: false,
                            allPages: response.last,
                            filter: filter,
                            notInGenres: notInGenres,
                            byRating: byRating
                        } :
                        {
                            podcasts: response.content,
                            loading: false,
                            allPages: response.last,
                            filter: filter,
                            notInGenres: notInGenres,
                            byRating: byRating,
                            currPage: 0
                        }
                );
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    submitFilter = (filter, notInGenres, byRating) => {
        this.loadPodcasts(filter, notInGenres, byRating, 0);
    };

    render() {
        return (
            <div>
                <Navbar isSearchOff={true} history={this.props.history}/>
                <div className="container-fluid">
                    <div style={{display: "flex",}}>
                        <div style={{width: "25%"}}>
                            <SearchPanel genres={this.state.genres}
                                         onSubmit={(filter, notInGenres, byRating) => this.submitFilter(filter, notInGenres, byRating)}
                                         filterValue={this.props.location.state ? this.props.location.state.filterValue : ""}
                            />
                        </div>
                        <div style={{width: "50%"}}>
                            {this.state.loadData === 1 &&
                            <PodcastList history={this.props.history} podcasts={this.state.podcasts}
                                         genres={this.state.genres}/>}
                        </div>
                        <div style={{width: "25%"}}>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}