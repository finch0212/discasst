import Navbar from '../Navbar';
import PodcastPageContent from "../podcastPage/PodcastPageContent";

const React = require('react');

export default class FeedPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div className="container-fluid">
                    <div style={{display: "flex",}}>
                        <div style={{width: "25%"}}>

                        </div>

                        <div style={{width: "50%", textAlign: "center"}}>
                            <PodcastPageContent
                                history={this.props.history}
                                feed={true}
                            />
                        </div>

                        <div style={{width: "25%"}}>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}