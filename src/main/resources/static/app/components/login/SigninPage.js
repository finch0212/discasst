const React = require('react');
import {getFullCSRFParam, saveAllCookie} from "../Helper";

export default class SigninPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            username: '',
            logout: false,
            error: false
        };
    }

    refreshPageParams(url) {
        this.setState({
            logout: false,
            error: false
        });
        if (url.searchParams.get("error") !== null) this.setState({error: true});
        if (url.searchParams.get("logout") !== null) this.setState({logout: true});
        if (!this.state.error && !this.state.logout) return false;
    }

    componentDidMount() {
        let url = new URL(window.location.href);
        this.refreshPageParams(url);
        $('form').submit(false);
    }

    signin() {
        fetch(`/perform_login?` +
            `username=${this.state.username}` + `&` +
            `password=${this.state.password}` + `&` +
            getFullCSRFParam(), {
            method: 'POST'
        }).then((response) => {
            if (!this.refreshPageParams(new URL(response.url))) {
                if (!this.state.error)
                    this.loadUserInfoFromServerAndRedirect();
            }
        });
    }

    onChangePass(event) {
        this.setState({password: event.target.value});
    }

    onChangeUsername(event) {
        this.setState({username: event.target.value});
    }

    loadUserInfoFromServerAndRedirect() {
        $.getJSON(`/getUserInfo?username=${this.state.username}`, (data) => {
            saveAllCookie(data.username, data.id, data.email, data.creationDate, data.name, data.bio)
        }).then(() => {
            this.props.history.push('/');
        });
    }

    toRegiser = () => {
        this.props.history.push('/register');
    };

    render() {
        return (
            <div className="center-block main-center signin-css">
                <form onSubmit={this.signin.bind(this)} className="form-signin center-block" style={{maxWidth: 320}}>
                    <hr className="hr100"/>
                    <hr className="hr60"/>
                    <img src="/images/discasst.png" type="image" alt="" width="260" className="center-block"></img>
                    <hr className="hr15"/>
                    <input type="username" className="form-control form-control-signin-css" placeholder="Username"
                           required id='signinUser'
                           autoFocus onChange={this.onChangeUsername.bind(this)}></input>
                    <input type="password" className="form-control form-control-signin-css" placeholder="Password"
                           required id='signinPass'
                           onChange={this.onChangePass.bind(this)}></input>
                    <br/>
                    <button type="submit" className="btn btn-lg btn-info btn-block">Sign In</button>
                    <br/>
                    <button type="button" className="btn btn-lg btn-info btn-block" onClick={() => {
                        this.toRegiser()
                    }}>Sign Up
                    </button>
                    <br/>
                    {this.state.logout &&
                    <div className="alert alert-info text-center">You signed out successfully.</div>}
                    {this.state.error &&
                    <div className="alert alert-danger text-center">Invalid Username or Password.</div>}
                </form>
            </div>
        )
    }
}