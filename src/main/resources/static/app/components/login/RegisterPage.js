const React = require('react');
import {getFullCSRFParam} from "../Helper";

var errorEnum = {
    NONE: "",
    UNKNOWN: "Unknown error. Try again.",
    USERNAME_TAKEN: "This login is already taken.",
    LONG_USERNAME: "Login is too long (max length: 16).",
    LONG_NAME: "Name is too long (max length: 16).",
    WRONG_NAME: "Wrong name (allowed characters: a-Z, а-Я, 0-9, ' ').",
    WRONG_USERNAME: "Wrong login (allowed characters: a-Z, 0-9, _).",
    WRONG_EMAIL: "Wrong email (exemple: abc-42@xyz.com).",
    WRONG_PASS: "Wrong password (allowed characters: a-Z, а-Я, 0-9).",
    WRONG_PASS_LENGTH: "Password length must be 4-20 characters.",
    WRONG_PASS_REPEAT: "Passwords don't match.",
};

export default class RegisterPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            username: '',
            name: '',
            email: '',
            success: false,
            error: errorEnum.NONE
        };
    }

    componentDidMount() {
        $('form').submit(false);
    }

    onChangeUsername(event) {
        this.setState({username: event.target.value});
    }

    onChangePass(event) {
        this.setState({password: event.target.value});
    }

    onChangeName(event) {
        this.setState({name: event.target.value});
    }

    onChangeEmail(event) {
        this.setState({email: event.target.value});
    }

    register() {
        if (!this.fullCheckPass()) return;
        fetch(`/addUser?` + `username=${this.state.username}` + `&` +
            `password=${this.state.password}` + `&` + `name=${this.state.name}` + `&` +
            `email=${this.state.email}` + `&` + getFullCSRFParam(), {
            method: 'POST'
        }).then((response) => {
            let status = response.headers.get("newUserStatus");
            if (status === "ok") {
                this.setState({success: true, error: errorEnum.NONE});
                setTimeout(() => {
                    this.props.history.push(`/signin`);
                }, 1500)
            }
            else if (status === "usernameError") {
                this.setState({success: false, error: errorEnum.USERNAME_TAKEN});
                document.getElementById("login").focus();
            }
            else this.setState({success: false, error: errorEnum.UNKNOWN});
        });
    }

    fullCheckPass() {
        this.setState({error: errorEnum.NONE});
        if (!this.validUsername()) return false;
        if (!this.validName()) return false;
        if (!this.validPassword()) return false;
        if (!this.validEmail()) return false;
        return true;
    }

    validUsername() {
        var usernameRegex = /^[a-zA-Z0-9_]+$/;
        var login = this.state.username;
        if (login.length > 16) {
            this.setState({error: errorEnum.LONG_USERNAME});
            document.getElementById("login").focus();
            return false;
        }
        var res = login.match(usernameRegex);
        if (res === null) {
            this.setState({error: errorEnum.WRONG_USERNAME});
            document.getElementById("login").focus();
            return false;
        }
        return true;
    }

    validPassword() {
        var passRegex = /^[a-zA-Zа-яА-Я0-9]+$/;
        var pass = this.state.password;
        if (pass.length > 20 || pass.length < 4) {
            this.setState({error: errorEnum.WRONG_PASS_LENGTH});
            document.getElementById("pass").focus();
            return false;
        }
        var res = pass.match(passRegex);
        if (res === null) {
            this.setState({error: errorEnum.WRONG_PASS});
            document.getElementById("pass").focus();
            return false;
        }
        return true;
    }

    validName() {
        var nameRegex = /^[a-zA-Zа-яА-Я0-9 ]+$/;
        var name = this.state.name;
        if (name.length > 16) {
            this.setState({error: errorEnum.LONG_NAME});
            document.getElementById("name").focus();
            return false;
        }
        var res = name.match(nameRegex);
        if (res === null) {
            this.setState({error: errorEnum.WRONG_NAME});
            document.getElementById("name").focus();
            return false;
        }
        return true;
    }

    validEmail() {
        var emailRegex = /^[a-z0-9-\\.]+@[a-z-]+.[a-z]+$/;
        var email = this.state.email;
        var res = email.match(emailRegex);
        if (res === null) {
            this.setState({error: errorEnum.WRONG_EMAIL});
            document.getElementById("email").focus();
            return false;
        }
        return true;
    }

    render() {
        const tss = this.state.success;
        const tse = this.state.error;
        return (
            <div className="center-block main-center" style={{maxWidth: 500}}>
                <hr className="hr100"/>
                <div className="panel panel-info">
                    <div className="panel-body center-block" style={{maxWidth: 400}}>
                        <form onSubmit={this.register.bind(this)} className="form-signin">
                            <hr className="hr10"/>
                            <div className="input-group">
                                <span className="input-group-addon">Login</span>
                                <input id="login" className="form-control" required autoFocus
                                       onChange={this.onChangeUsername.bind(this)}/>
                            </div>
                            <hr className="hr15"/>
                            <div className="input-group">
                                <span className="input-group-addon">Password</span>
                                <input id="pass" type="password" className="form-control" required
                                       onChange={this.onChangePass.bind(this)}/>
                            </div>
                            <hr className="hr15"/>
                            <div className="input-group">
                                <span className="input-group-addon">Name</span>
                                <input id="name" className="form-control" required
                                       onChange={this.onChangeName.bind(this)}/>
                            </div>
                            <hr className="hr15"/>
                            <div className="input-group">
                                <span className="input-group-addon">Email</span>
                                <input id="email" className="form-control" required
                                       onChange={this.onChangeEmail.bind(this)}/>
                            </div>
                            <hr className="hr15"/>
                            {!tss && <button type="submit" className="btn btn-lg btn-info btn-block">Register</button>}
                            {tss && <button type="button" className="btn btn-lg btn-info btn-block"
                                            disabled>Register</button>}
                            <hr className="hr10"/>
                            {tss &&
                            <div className="alert alert-success text-center">You successfully registered. Go to Sign
                                in...</div>}
                            {tse === errorEnum.USERNAME_TAKEN &&
                            <div className="alert alert-danger text-center">{errorEnum.USERNAME_TAKEN}</div>}
                            {tse === errorEnum.UNKNOWN &&
                            <div className="alert alert-danger text-center">{errorEnum.UNKNOWN}</div>}
                            {tse === errorEnum.LONG_USERNAME &&
                            <div className="alert alert-danger text-center">{errorEnum.LONG_USERNAME}</div>}
                            {tse === errorEnum.WRONG_USERNAME &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_USERNAME}</div>}
                            {tse === errorEnum.LONG_NAME &&
                            <div className="alert alert-danger text-center">{errorEnum.LONG_NAME}</div>}
                            {tse === errorEnum.WRONG_NAME &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_NAME}</div>}
                            {tse === errorEnum.WRONG_EMAIL &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_EMAIL}</div>}
                            {tse === errorEnum.WRONG_PASS &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_PASS}</div>}
                            {tse === errorEnum.WRONG_PASS_REPEAT &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_PASS_REPEAT}</div>}
                            {tse === errorEnum.WRONG_PASS_LENGTH &&
                            <div className="alert alert-danger text-center">{errorEnum.WRONG_PASS_LENGTH}</div>}
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}