import React, {Component} from 'react';
import {Panel} from 'react-bootstrap';
import {stats, statTime} from './StatisticsPage';

export default class StatisticsPanel extends Component {
    constructor() {
        super();
        this.state = {}
    }

    getStat() {
        if (this.props.stat === stats.USER) return "Новых пользователей:";
        if (this.props.stat === stats.PODCAST) return "Подкастов создано:";
        if (this.props.stat === stats.EPISODE) return "Выпусков загружено:";
    }

    getTime() {
        if (this.props.time === statTime.MONTH) return "ЗА МЕСЯЦ";
        if (this.props.time === statTime.DAY) return "СЕГОДНЯ";
    }

    getLabelClass() {
        if (this.props.time === statTime.MONTH) return "label label-info";
        if (this.props.time === statTime.DAY) return "label label-success";
    }

    render() {
        return (

            <Panel style={{width: 320, margin: 10}}>
                <Panel.Body>
                    <div className="text-center" style={{display: "flex", justifyContent: "center", flexWrap: "wrap"}}>
                        <h3>
                            <div className={this.getLabelClass()}>{this.getTime()}</div>
                        </h3>
                        <h3>{this.getStat()}</h3>
                        <Panel style={{width: "50%", backgroundColor: "#f1f1f1"}}>
                            <Panel.Body>
                                <h1 style={{margin: 0}}><strong><u>{this.props.number}</u></strong></h1>
                            </Panel.Body>
                        </Panel>
                    </div>
                </Panel.Body>
            </Panel>
        );
    }
}
