import React, {Component} from 'react';
import StatisticsPanel from './StatisticsPanel';
import Navbar from "../Navbar";

export default class StatisticsPage extends Component {
    constructor() {
        super();
        this.state = {
            usersPerDay: '...',
            usersPerMonth: '...',
            podcastPerDay: '...',
            podcastPerMonth: '...',
            episodePerDay: '...',
            episodePerMonth: '...',
        }
    }

    componentDidMount() {
        this.loadData();
    }

    loadData() {
        fetch(`/getEpisodesCountToday`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({episodePerDay: json});
            });
        fetch(`/getEpisodesCountThisMounth`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({episodePerMonth: json});
            });
        fetch(`/getPodcastsCountToday`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({podcastPerDay: json});
            });
        fetch(`/getPodcastsCountThisMounth`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({podcastPerMonth: json});
            });
        fetch(`/getUsersCountToday`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({usersPerDay: json});
            });
        fetch(`/getUsersCountThisMounth`)
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                this.setState({usersPerMonth: json});
            });
    }

    render() {
        const ts = this.state;
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div>
                    <div style={{display: "flex", justifyContent: "center", flexWrap: "wrap"}}>
                        <StatisticsPanel time={statTime.MONTH} stat={stats.USER} number={ts.usersPerMonth}/>
                        <StatisticsPanel time={statTime.MONTH} stat={stats.EPISODE} number={ts.episodePerMonth}/>
                        <StatisticsPanel time={statTime.MONTH} stat={stats.PODCAST} number={ts.podcastPerMonth}/>
                    </div>
                    <div style={{display: "flex", justifyContent: "center", flexWrap: "wrap"}}>
                        <StatisticsPanel time={statTime.DAY} stat={stats.USER} number={ts.usersPerDay}/>
                        <StatisticsPanel time={statTime.DAY} stat={stats.EPISODE} number={ts.episodePerDay}/>
                        <StatisticsPanel time={statTime.DAY} stat={stats.PODCAST} number={ts.podcastPerDay}/>
                    </div>
                </div>
            </div>
        );
    }
}

const statTime = {
    MONTH: 0,
    DAY: 1
};

const stats = {
    USER: 0,
    PODCAST: 1,
    EPISODE: 2,
};

export {stats, statTime};