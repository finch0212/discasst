const React = require('react');
import {deleteAllCookie, getCookie, getFullCSRFParam} from "./Helper";

export default class Navbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isSignedIn: false
        };
    }

    componentDidMount() {
        this.isUserLogIn();
    }

    loginout = () => {
        if (this.state.isSignedIn) {
            deleteAllCookie();
            this.props.history.push(`/signin?logout`);
        }
        else {
            this.props.history.push(`/signin`);
        }
    };

    toStats = () => {
        this.props.history.push(`/statistics`);
    };

    toMyProfile = () => {
        this.props.history.push(`/user/${getCookie("username")}`);
    };

    toFeed = () => {
        this.props.history.push(`/feed`);
    };

    toNotifications = () => {
        this.props.history.push(`/notifications`);
    };

    toHome = () => {
        this.props.history.push(`/`);
    };

    toSignIn = () => {
        this.props.history.push(`/signin`);
    };

    isUserLogIn() {
        let username = getCookie("username");
        if (username === "" || username === undefined) return false;
        fetch(`/loginCheck?${getFullCSRFParam()}`, {
            method: 'POST'
        }).then((response) => {
            if (response.status === 200 && !response.redirected) this.setState({isSignedIn: true});
        });
    }

    onFilterSubmit = () => {
        const filter = document.getElementById("topSearch").value;
        this.props.history.push({
            pathname: '/search',
            state: {filterValue: filter}
        })
    };

    render() {
        return (
            <div>
                <nav className="navbar navbar-default" style={{backgroundColor: '#ffffff'}} role="navigation">
                    <div className="container-fluid centered">
                        <div className="navbar-header">
                            <a onClick={() => {
                                this.toHome()
                            }} className="navbar-brand" style={{height: '64px', display: 'flex', alignItems: 'center'}}>
                                <img src="/images/discasst.png" height='36'></img>
                            </a>
                        </div>
                        <div>
                            <ul className="nav navbar-nav"
                                style={{height: '64px', display: 'flex', alignItems: 'center'}}>
                                <li><a onClick={() => {
                                    !this.state.isSignedIn ? this.toSignIn() : this.toFeed()
                                }}>
                                    <span className="glyphicon glyphicon-align-justify"></span> Feed</a>
                                </li>

                                <li><a onClick={() => {
                                    !this.state.isSignedIn ? this.toSignIn() : this.toMyProfile()
                                }}>
                                    <span className="glyphicon glyphicon-user"></span> My Profile</a>
                                </li>

                                <li><a onClick={() => {
                                    this.toStats()
                                }}>
                                    <span className="glyphicon glyphicon-stats"></span></a>
                                </li>
                            </ul>

                            {!this.props.isSearchOff && <ul className="nav navbar-nav navbar-center">
                                <li>
                                    <a>
                                        <form onSubmit={this.onFilterSubmit}>
                                            <div className="input-group" style={{width: '300px'}}>
                                                <input id="topSearch" type="text" className="form-control"/>
                                            <span className="input-group-btn">
                                                <button className="btn btn-info" type="button"
                                                        onClick={this.onFilterSubmit}
                                                >
                                                    <span className="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                        </div>
                                        </form>
                                    </a>
                                </li>
                            </ul>}

                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <a><button className="btn btn-info" type="button" onClick={this.loginout}>
                                        <span
                                            className="glyphicon glyphicon-log-out"></span> {this.state.isSignedIn ? "Sign Out" : "Sign In"}
                                    </button></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        )
    }
}