import React, {Component} from 'react';
import Navbar from "../Navbar";
import {getCookie, getFullCSRFParam} from '../Helper'

export default class CreateNewEpisodePage extends Component {
    constructor() {
        super();
        this.state = {}
    }

    createNewEpisode = () => {
        if (this.validationIsOk()) {
            console.log("Create new episode")
            let obj = {
                "name": this.refs.episodeName.value,
                "podcastId": this.props.match.params.podcastId,
                "description": this.refs.episodeDesc.value,
                "podsterLink": this.refs.podsterLink.value,
            };
            let episode = JSON.stringify(obj);
            const that = this;
            $.ajax({
                url: `/createEpisode?${getFullCSRFParam()}`,
                type: 'post',
                contentType: 'application/json',
                data: episode,
                success: () => {
                    that.episodeIsCreated();
                },
                error: (jqXhr, textStatus, errorThrown) => {
                    console.log(errorThrown + " " + textStatus);
                }
            });
        }
    };

    episodeIsCreated() {
        console.log("Episode is created");
        this.props.history.push(`/user/${getCookie("username")}`);
    }

    validationIsOk() {
        let en = this.refs.episodeName.value;
        let ed = this.refs.episodeDesc.value;
        let pl = this.refs.podsterLink.value;
        if (en !== "" && ed !== "" && pl !== "") return true;
        else return false;
    }

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <div className="container-fluid" style={{width: "50%"}}>
                        <br/><br/><br/>
                        <div className="panel panel-default">
                            <div className="panel-body" style={{maxWidth: '100%'}}>
                                <input ref="episodeName" type="text" maxLength="50" className="form-control"
                                       placeholder="* Название"/><br/>
                                <textarea ref="episodeDesc" className="form-control" maxLength="350"
                                          style={{resize: "vertical"}} rows="5"
                                          placeholder="* Описание"></textarea><br/>
                                <input ref="podsterLink" type="text" maxLength="50" className="form-control"
                                       placeholder="* Ссылка на выпуск в Podster.fm (формата https://podcast-name.podster.fm/42)"/><br/>
                                <br/>
                                <div>
                                    <button type="button" className="btn btn-info pull-right"
                                            onClick={this.createNewEpisode}><span
                                        className="glyphicon glyphicon-plus"></span> Добавить новый выпуск
                                    </button>
                                </div>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}