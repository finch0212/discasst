import React from 'react';
import UserProfilePageModalOnDelete from './UserProfilePageModalOnDelete';
import {getCookie, getFullCSRFParam} from '../Helper';

export default class UserProfilePagePodcastTable extends React.Component {
    constructor() {
        super();
        this.state = {
            podcasts: -1,
            coauthorPodcast: -1,
            deletedPodcast: null
        };
    }

    componentDidMount() {
        if (this.props.userId !== '-1') this.getMyPodcasts();
    }

    getMyPodcasts() {
        fetch(`/getPodcastsByUserId?id=${this.props.userId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                this.setState({podcasts: json});
                this.getCoauthorPodcasts();
            });
    }

    getCoauthorPodcasts() {
        fetch(`/getPodcastsByUserIdInCoauthors?id=${this.props.userId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                this.setState({coauthorPodcast: json});
            });
    }

    addEpisode = (item) => {
        console.log("add episode to " + item.name);
        this.props.history.push(`/createNewEpisode/${item.id}`);
    };

    deletePodcast = () => {
        console.log("delete podcast " + this.state.deletedPodcast.name);
        $.ajax({
            url: `/removePodcast?id=${this.state.deletedPodcast.id}&${getFullCSRFParam()}`,
            type: 'post',
            success: () => {
                this.props.history.push(`/user/${getCookie("username")}`);
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
        window.location.reload();
    };

    chooseDeletedPodcast = (item) => {
        this.setState({deletedPodcast: item});
    };

    addNewPodcast = () => {
        console.log("add new podcast");
        this.props.history.push(`/createNewPodcast`);
    };

    editCoauthors = (item) => {
        this.props.history.push(`/addCoauthors/${item.id}`);
    };

    render() {
        return (
            <div className="panel panel-info" style={{backgroundColor: "#F9F9F9"}}>
                <div className="panel-body">
                    {this.state.podcasts.length !== 0 && <h4>Созданные:</h4>}
                    {this.state.podcasts !== -1 && this.state.podcasts.map((item, num) =>
                        <div key={item.id}>
                            <div className="panel panel-info">
                                <div className="panel-body">
                                    {this.props.myPage &&
                                    <div style={{display: "flex", justifyContent: "center"}}>
                                        <p className="h4" style={{width: "100%"}}><a onClick={() => {
                                            this.props.history.push(`/podcast/${item.id}`)
                                        }}>{item.name}</a></p>
                                        <button className="btn btn-info btn"
                                                onClick={() => this.editCoauthors(item)}><span
                                            className="glyphicon glyphicon-user"></span> Изменить соавторов
                                        </button>
                                        &#160;&#160;
                                        <button className="btn btn-info btn" onClick={() => this.addEpisode(item)}><span
                                            className="glyphicon glyphicon-plus"></span> Добавить выпуск
                                        </button>
                                        &#160;&#160;
                                        <button className="btn btn-danger btn" data-toggle="modal"
                                                data-target="#exampleModal"
                                                onClick={() => this.chooseDeletedPodcast(item)}
                                        >
                                            <span className="glyphicon glyphicon-remove"></span>
                                        </button>
                                    </div>}
                                    {!this.props.myPage &&
                                    <div style={{display: "flex", justifyContent: "center"}}>
                                        <p className="h4" style={{width: "100%"}}><a onClick={() => {
                                            this.props.history.push(`/podcast/${item.id}`)
                                        }}>{item.name}</a></p>
                                    </div>
                                    }
                                </div>
                            </div>
                        </div>
                    )}
                    {this.state.coauthorPodcast.length !== 0 && <h4>В соавторстве:</h4>}
                    {this.state.coauthorPodcast !== -1 && this.state.coauthorPodcast.map((item, num) =>
                        <div key={item.id}>
                            <div className="panel panel-info">
                                <div className="panel-body">
                                    <div style={{display: "flex", justifyContent: "center"}}>
                                        <p className="h4" style={{width: "100%"}}><a onClick={() => {
                                            this.props.history.push(`/podcast/${item.id}`)
                                        }}>{item.name}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                    {this.props.myPage &&
                    <button style={{height: "39px"}} className="btn btn-info pull-right"
                            onClick={this.addNewPodcast}>
                        <span className="glyphicon glyphicon-plus"></span> Добавить новый подкаст
                    </button>}
                </div>
                <UserProfilePageModalOnDelete onDelete={this.deletePodcast} podcast={this.state.deletedPodcast}/>
            </div>
        );
    }
}
