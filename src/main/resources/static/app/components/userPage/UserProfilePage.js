import UserProfilePagePodcastTable from "./UserProfilePagePodcastTable";
import Navbar from "../Navbar";
import {getCookie, getFullCSRFParam, saveCookie} from "../Helper";

const React = require('react');

export default class UserProfilePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            myPage: false,
            editMode: false,
            user: null,
            loadEnds: false,
            isSignedIn: false
        };
    }

    componentDidMount() {
        let isSignedIn = false;
        fetch(`/loginCheck?${getFullCSRFParam()}`, {
            method: 'POST'
        }).then((response) => {
            if (response.status === 200 && !response.redirected) {
                let username = getCookie("username");
                if (username !== "" && username !== undefined) this.setState({isSignedIn: true});
            }
            if (this.props.match.params.username === getCookie("username") && this.state.isSignedIn) {
                console.log('myPage');
                this.setState({myPage: true, loadEnds: true});
            } else {
                console.log('not myPage');
                this.loadUserInfo()
            }
        });
    }

    isUserLogIn() {
        fetch(`/loginCheck?${getFullCSRFParam()}`, {
            method: 'POST'
        }).then((response) => {
            if (response.status === 200 && !response.redirected) this.setState({isSignedIn: true});
        });
    }

    onEditClick() {
        this.setState({editMode: true});
    }

    onEditOkClick() {
        this.changeName();

    }

    changeName() {
        if (this.refs.newName.value !== "")
            $.ajax({
                url: `/updateProfile?${getFullCSRFParam()}&username=${getCookie("username")}&name=${this.refs.newName.value}`,
                type: 'post',
                success: () => {
                    saveCookie("name", this.refs.newName.value);
                    this.setState({editMode: false});
                },
                error: (jqXhr, textStatus, errorThrown) => {
                    console.log(errorThrown + " " + textStatus);
                }
            });
    }

    loadUserInfo() {
        $.ajax({
            url: `/getUserInfo?username=${this.props.match.params.username}`,
            type: 'get',
            success: (response) => {
                console.log(response);
                console.log(response);
                this.setState({user: response, loadEnds: true});
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    }

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div className="container-fluid">
                    <div style={{display: "flex", justifyContent: "center", paddingTop: 30}}>
                        <div className="panel panel-info" style={{width: "50%"}}>
                            <div className="panel-body" style={{width: "100%"}}>
                                <div className="container-fluid">
                                    <h2 className="text-center">@{this.state.myPage ? getCookie("username") : this.state.user !== null ? this.state.user.username : '...'}</h2>
                                    <hr/>
                                    <h4>id: <font
                                        color="#A2A2A2"> {this.state.myPage ? getCookie("userId") : this.state.user !== null ? this.state.user.id : '...'}</font>
                                    </h4>
                                    {!this.state.editMode &&
                                    <h4>Имя:
                                        <font
                                            color="#A2A2A2"> {this.state.myPage ? getCookie("name") : this.state.user !== null ? this.state.user.name : '...'}</font>
                                        &#160;
                                        {this.state.myPage &&
                                        <button className="btn btn-info btn-sm" type="button" onClick={() => {
                                            this.onEditClick()
                                        }}>
                                            <span className="glyphicon glyphicon-pencil"></span>
                                        </button>
                                        }
                                    </h4>}
                                    {this.state.editMode &&
                                    <h4>Имя:
                                        <input ref="newName" placeholder={'Введите имя'} maxLength={30}/>
                                        &#160;
                                        <button className="btn btn-info btn-sm" type="button" onClick={() => {
                                            this.onEditOkClick()
                                        }}>
                                            <span className="glyphicon glyphicon-ok"></span>
                                        </button>
                                    </h4>}
                                    <h4>Email:<font
                                        color="#A2A2A2"> {this.state.myPage ? getCookie("userEmail") : this.state.user !== null ? this.state.user.email : '...'}</font>
                                    </h4>
                                    <hr/>
                                    <h4>Подкасты:</h4>
                                    {this.state.loadEnds &&
                                    <UserProfilePagePodcastTable
                                        myPage={this.state.myPage}
                                        userId={this.state.myPage ? getCookie("userId") : this.state.user !== null ? this.state.user.id : '-1'}
                                        history={this.props.history}
                                    />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}