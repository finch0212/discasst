import React, {Component} from 'react';
import Navbar from "../Navbar";
import CoauthorsList from "./CoauthorsList";
import {getCookie, getFullCSRFParam} from "../Helper";

export default class AddCoauthorsPage extends Component {
    constructor() {
        super();
        this.state = {
            coauthors: []
        }
    }

    componentDidMount() {
        this.loadCoauthors();
    }

    loadCoauthors() {
        fetch(`/getCoauthors?podcastId=${this.props.match.params.podcastId}`)
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                console.log(json)
                let coauthorsIds = json.map(x => x.userId.toString());
                this.setState({coauthors: coauthorsIds});
            });
    }

    isId(x) {
        return (x >= 0 && x % 1 === 0 && x.indexOf('.') === -1 && x.length <= 10 && x !== "")
    }

    addCoauthor = () => {
        let tmp = this.state.coauthors;
        let cid = this.refs.coauthorInput.value;
        if (cid !== getCookie('userId'))
            if (tmp.length <= 3 && this.isId(cid)) {
                let newCoauthor = cid;
                console.log(tmp.indexOf(newCoauthor));
                if (tmp.indexOf(newCoauthor) < 0) {
                    this.refs.coauthorInput.value = "";
                    tmp = [...tmp, newCoauthor];
                    this.setState({coauthors: tmp});
                }
            }
    };

    deleteCoauthor = (item) => {
        let tmp = this.state.coauthors;
        let index = tmp.indexOf(item);
        tmp.splice(index, 1);
        this.setState({coauthors: tmp});
    };

    saveChanges = () => {
        let authorsIds = JSON.stringify(this.state.coauthors);
        const that = this;
        $.ajax({
            url: `/addCoauthors?podcastId=${this.props.match.params.podcastId}&${getFullCSRFParam()}`,
            type: 'post',
            contentType: 'application/json',
            data: authorsIds,
            success: () => {
                that.changesIsApplyed();
            },
            error: (jqXhr, textStatus, errorThrown) => {
                console.log(errorThrown + " " + textStatus);
            }
        });
    };

    changesIsApplyed = () => {
        this.props.history.push(`/user/${getCookie("username")}`);
    };

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <div className="container-fluid" style={{width: "50%"}}>
                        <br/><br/><br/>
                        <div className="panel panel-default">
                            <div className="panel-body" style={{maxWidth: '100%'}}>
                                {this.state.coauthors.length <= 3 ?
                                    <div style={{display: "flex", justifyContent: "center"}}>
                                        <input ref="coauthorInput" maxLength="10" className="form-control"
                                               placeholder="Id соведущего" type="number" min="0" step="1"
                                               max="999999999"/>&#160;&#160;
                                        <button type="button" className="btn btn-info"
                                                onClick={this.addCoauthor}>Добавить ведущего
                                        </button>
                                    </div> : <h4>Соведущие:</h4>}
                                {this.state.coauthors.length > 0 && <br/>}
                                <CoauthorsList coauthors={this.state.coauthors} onClick={this.deleteCoauthor}/>
                                <br/>
                                <button
                                    type="button"
                                    className="btn btn-info"
                                    onClick={this.saveChanges}>Сохранить
                                </button>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}