import React, {Component} from 'react';

export default class CoauthorsList extends Component {
    constructor() {
        super();
        this.state = {}

    }

    render() {
        return (<div className="container-fluid">
                <div style={{display: "flex", justifyContent: "flex-start", flexWrap: "wrap"}}>

                    {this.props.coauthors.map((item) =>
                        <button key={item} onClick={() => this.props.onClick(item)} style={{margin: "2px"}}
                                className="btn btn-info">id: {item} <span className="glyphicon glyphicon-remove"></span>
                        </button>
                    )}
                </div>
            </div>
        );
    }
}