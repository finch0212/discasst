import React, {Component} from 'react';
import Navbar from "../Navbar";
import {getCookie, getFullCSRFParam} from "../Helper";

export default class CreateNewPodcastPage extends Component {
    constructor() {
        super();
        this.state = {
            genres: null,
            selectedGenre: {
                id: null,
                name: "* Выберите"
            },
        };
    }

    componentDidMount() {
        this.getGenres();
    }

    getGenres = () => {
        let genres;
        $.getJSON(`/getGenres`, (data) => {
            console.log(data);
            this.setState({genres: data});
            return data;
        });
    };

    selectGenre = (genre) => {
        this.setState({selectedGenre: genre});
    };

    validationIsOk() {
        let pn = this.refs.podcastName.value;
        let pd = this.refs.podcastDesc.value;
        let g = this.state.selectedGenre.name;
        if (pn !== "" && pd !== "" && g !== "* Выберите") return true;
        return false;
    }

    createNewPodcast = () => {
        if (this.validationIsOk()) {
            console.log("Create new podcast");

            let obj = {
                "name": this.refs.podcastName.value,
                "authorId": getCookie("userId"),
                "description": this.refs.podcastDesc.value,
                "genreId": (this.state.selectedGenre.id - 1),
                "coauthors": this.state.coauthors,
                "vkLink": this.refs.vkLink.value,
                "twitterLink": this.refs.twitterLink.value,
                "itunesLink": this.refs.itunesLink.value,
                "facebookLink": this.refs.facebookLink.value,
                "rating": 0
            };
            let podcast = JSON.stringify(obj);
            const that = this;
            $.ajax({
                url: `/createPodcast?${getFullCSRFParam()}`,
                type: 'post',
                contentType: 'application/json',
                data: podcast,
                success: () => {
                    that.podcastIsCreated();
                },
                error: (jqXhr, textStatus, errorThrown) => {
                    console.log(errorThrown + " " + textStatus);
                }
            });

        }
    };

    podcastIsCreated() {
        console.log("Подкаст создан!")
        this.props.history.push(`/user/${getCookie("username")}`);
    }

    render() {
        return (
            <div>
                <Navbar history={this.props.history}/>
                <div style={{display: "flex", justifyContent: "center"}}>
                    <div className="container-fluid" style={{width: "50%"}}>
                        <br/><br/><br/>
                        <div className="panel panel-default">
                            <div className="panel-body" style={{maxWidth: '100%'}}>
                                <input ref="podcastName" type="text" maxLength="50" className="form-control"
                                       placeholder="* Название"/><br/>
                                <textarea ref="podcastDesc" className="form-control" maxLength="350"
                                          style={{resize: "vertical"}} rows="5"
                                          placeholder="* Описание"></textarea><br/>
                                <div className="btn-group">
                                    <button className="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                            style={{minWidth: "160px"}}>{"Жанр: " + this.state.selectedGenre.name}&#160;&#160;
                                        <span className="glyphicon glyphicon-chevron-down"></span></button>
                                    {this.state.genres !== null && <ul className="dropdown-menu">
                                        {this.state.genres.map((item) =>
                                            <li key={item.id}><a role="button"
                                                                 onClick={() => this.selectGenre(item)}>{item.name}</a>
                                            </li>
                                        )}
                                    </ul>}
                                </div>
                                <br/><br/>
                                <input ref="twitterLink" type="text" maxLength="50" className="form-control"
                                       placeholder="Ссылка на Twitter"/><br/>
                                <input ref="vkLink" type="text" maxLength="50" className="form-control"
                                       placeholder="Ссылка на Vk"/><br/>
                                <input ref="facebookLink" type="text" maxLength="50" className="form-control"
                                       placeholder="Ссылка на Facebook"/><br/>
                                <input ref="itunesLink" type="text" maxLength="50" className="form-control"
                                       placeholder="Ссылка на iTunes"/><br/>
                                <div>
                                    <button type="button" className="btn btn-info pull-right"
                                            onClick={this.createNewPodcast}><span
                                        className="glyphicon glyphicon-plus"></span> Создать новый подкаст
                                    </button>
                                </div>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}