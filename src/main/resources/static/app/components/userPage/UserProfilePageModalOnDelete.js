import React, {Component} from 'react';

export default class UserProfilePageModalOnDelete extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    getName() {
        if (this.props.podcast) return this.props.podcast.name;
        return null
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            Удалить подкаст "{this.getName()}"?
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Нет, я передумал
                            </button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal"
                                    onClick={this.props.onDelete}>Да, удалить подкаст
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}