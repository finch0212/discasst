import SigninPage from "./components/login/SigninPage";
import RegisterPage from "./components/login/RegisterPage"
import SearchPage from "./components/searchPage/SearchPage"
import UserProfilePage from "./components/userPage/UserProfilePage"
import PodcastPage from "./components/podcastPage/PodcastPage";
import CreateNewPodcastPage from "./components/userPage/CreateNewPodcastPage";
import CreateNewEpisodePage from "./components/userPage/CreateNewEpisodePage";
import StatisticsPage from "./components/statistics/StatisticsPage";
import AddCoauthorsPage from "./components/userPage/AddCoauthorsPage";
import FeedPage from "./components/feedPage/FeedPage";
import {BrowserRouter, Route, Switch} from 'react-router-dom';

const React = require('react');
const ReactDOM = require('react-dom');

export default class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={SearchPage}/>
                    <Route path='/signin' component={SigninPage}/>
                    <Route path='/register' component={RegisterPage}/>
                    <Route path='/search' component={SearchPage}/>
                    <Route path='/feed' component={FeedPage}/>
                    <Route path='/statistics' component={StatisticsPage}/>
                    <Route path='/createNewPodcast' component={CreateNewPodcastPage}/>
                    <Route path='/podcast/:podcastId' component={PodcastPage}/>
                    <Route path='/createNewEpisode/:podcastId' component={CreateNewEpisodePage}/>
                    <Route path='/user/:username' component={UserProfilePage}/>
                    <Route path='/addCoauthors/:podcastId' component={AddCoauthorsPage}/>
                </Switch>
            </BrowserRouter>
        )
    }
}

const el = document.getElementById('root');
ReactDOM.render(<App/>, el);