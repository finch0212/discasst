package discasst.da;

import discasst.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    List<User> findAllByIdIn(long[] list);

    List<User> findAllByCreationDate(String date);

    @Query("select a from User a where a.creationDate like :monthAndYear")
    List<User> findAllInThisMonth(@Param("monthAndYear") String monthAndYear);
}
