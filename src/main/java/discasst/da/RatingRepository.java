package discasst.da;

import discasst.entity.RatingEntity;
import org.springframework.data.repository.CrudRepository;

public interface RatingRepository extends CrudRepository<RatingEntity, Long> {
    RatingEntity findByPodcastIdAndUserId(long podcastId, long userId);

    void deleteAllByPodcastId(long id);
}
