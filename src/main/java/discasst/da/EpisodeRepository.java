package discasst.da;

import discasst.entity.Episode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EpisodeRepository extends CrudRepository<Episode, Long> {
    Page<Episode> findAllByPodcastIdOrderByIdDesc(long id, Pageable pageable);

    Page<Episode> findAllByPodcastIdInOrderByIdDesc(long[] podcastIds, Pageable pageable);

    List<Episode> findAllByDate(String date);

    void deleteAllByPodcastId(long id);

    @Query("select a from Episode a where a.date like :monthAndYear")
    List<Episode> findAllInThisMonth(
            @Param("monthAndYear") String monthAndYear);
}

