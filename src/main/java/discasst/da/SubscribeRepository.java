package discasst.da;

import discasst.entity.SubscribeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SubscribeRepository extends CrudRepository<SubscribeEntity, Long> {
    SubscribeEntity findByPodcastIdAndUserId(long podcastId, long userId);

    void deleteAllByPodcastId(long id);

    @Query("SELECT a.podcastId FROM SubscribeEntity a WHERE a.userId like :userId AND a.isSubscribed LIKE true")
    long[] findAllByUserId(@Param("userId") long userId);
}
