package discasst.da;

import discasst.entity.CoauthorsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CoauthorsRepository extends CrudRepository<CoauthorsEntity, Long> {
    List<CoauthorsEntity> findAllByPodcastId(long id);

    @Query("SELECT a.podcastId FROM CoauthorsEntity a WHERE a.userId like :userId")
    long[] findAllByUserId(@Param("userId") long userId);

    void deleteAllByPodcastId(long id);
}
