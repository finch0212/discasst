package discasst.da;

import discasst.entity.Podcast;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PodcastRepository extends PagingAndSortingRepository<Podcast, Long> {

    Podcast findById(long id);

    List<Podcast> findAll();

    Page<Podcast> findByNameContainsIgnoreCaseAndGenreIdNotInOrderByRatingDesc(String name, long[] genres, Pageable pageable);

    Page<Podcast> findByNameContainsIgnoreCaseAndGenreIdNotInOrderByIdDesc(String name, long[] genres, Pageable pageable);

    List<Podcast> findAllByAuthorId(long id);

    List<Podcast> findAllByIdIn(long[] ids);

    List<Podcast> findAllByCreationDate(String date);

    @Query("select a from Podcast a where a.creationDate like :monthAndYear")
    List<Podcast> findAllInThisMonth(@Param("monthAndYear") String monthAndYear);
}
