package discasst.da;

import discasst.entity.Tweet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TweetRepository extends PagingAndSortingRepository<Tweet, Long> {
    Page<Tweet> findAllByPodcastIdOrderByIdDesc(long id, Pageable pageable);

    Page<Tweet> findAllByPodcastIdInOrderByIdDesc(long[] podcastIds, Pageable pageable);

    void deleteAllByPodcastId(long id);
}