package discasst.da;

import discasst.entity.*;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

@Component
public class ExposeEntityIdRest extends RepositoryRestConfigurerAdapter {
    /**
     * Сохраняет айдишники в объектах
     */
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(User.class);
        config.exposeIdsFor(Podcast.class);
        config.exposeIdsFor(Genre.class);
        config.exposeIdsFor(Episode.class);
        config.exposeIdsFor(CoauthorsEntity.class);
        config.exposeIdsFor(RatingEntity.class);
        config.exposeIdsFor(Tweet.class);
    }
}

