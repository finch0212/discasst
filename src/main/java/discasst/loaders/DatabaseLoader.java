package discasst.loaders;

import discasst.da.*;
import discasst.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PodcastRepository podcastRepository;
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private EpisodeRepository episodeRepository;
    @Autowired
    private TweetRepository tweetRepository;
    @Autowired
    private CoauthorsRepository coauthorsRepository;

    @Override
    public void run(String... strings) {
        List<String> genres = Arrays.asList("Обо всем", "Авто", "Аудиокниги", "Бизнес и карьера", "Интернет и технологии"
                , "Кино и музыка", "Красота и здоровье", "Культура и искусство", "Наука и образование", "Политика"
                , "Отношения", "Путешествия", "Спорт", "Видеоигры");
        for (String name : genres) {
            this.genreRepository.save(new Genre(name));
        }

        this.userRepository.save(new User("finch", "sal0212@mail.ru", new BCryptPasswordEncoder().encode("finch"), "FINCH"));
        this.userRepository.save(new User("q", "qqq@mail.ru", new BCryptPasswordEncoder().encode("q"), "bigQ"));
        this.userRepository.save(new User("vedensky", "vedensky@gmail.com", new BCryptPasswordEncoder().encode("vedensky"), "Борис Веденский"));
        this.userRepository.save(new User("istishev", "istishev@mail.ru", new BCryptPasswordEncoder().encode("istishev"), "Валерий Истишев"));
        this.userRepository.save(new User("ilyaliya", "ilyaliya@gmail.com", new BCryptPasswordEncoder().encode("ilyaliya"), "Илья Рябов"));

        String podDesc = "Завтракаст один из самых популярных подкастов про игры, медиа, кино, сериалы и технологии в России. Его ведущими являются три очень разных человека маркетолог Тимур, технический директор Дима и видеопродюсер Максим. Их объединяет любовь к играм, книгам, сериалам, кино, новым технологиям, трендам в интернете и интересным событиям вокруг нас.";
        this.podcastRepository.save(new Podcast("Завтракаст", podDesc, 404, 13, 0));
        podDesc = "Раз в неделю Борис Веденский, Валерий Истишев и Илья Рябов соберутся, чтобы сжечь несколько ведьм, разоблачить пару заговоров, обсудить последние новости интернета, технологий, кино и видеоигр в неформальной обстановке.";
        this.podcastRepository.save(new Podcast("Droider Cast", podDesc, "https://itunes.apple.com/ru/podcast/droider-cast/id1057019371?mt=2", "https://vk.com/droider_ru", "https://twitter.com/vedensky", "https://www.facebook.com/Droider", 12, 4, 509, 217, 17));
        this.podcastRepository.save(new Podcast("Newочём", "Мы переводим и озвучиваем всё самое интересное, что появляется в зарубежном Интернете.", 342, 0, 0));
        podDesc = "Рассказываем обо всём, что можно «взломать»: о здоровье, работе и технологиях, об образовании, спорте и отдыхе, об отношениях, финансах и мотивации. Помогаем решить проблемы или упростить ситуации, с которыми вы сталкиваетесь каждый день. Отвечаем на вопросы и призываем посмотреть на привычные вещи другими глазами.";
        this.podcastRepository.save(new Podcast("Подкаст «Лайфхакера»", podDesc, 15, 0, 0));
        podDesc = "Наше психологическое радио о простых и сложных переживаниях, о психологии эмоций, личной эффективности и личностном развитии, о саногенном мышлении, мы рассказываем на простых примерах, как пережить те или иные эмоции, как то: страх, гнев, стыд, обида или вина, либо вообще их угасить. Наши подкастырассказывают как контролируемое спокойствие влияет на результаты в бизнесе, спорте, развитии личности и личной жизни. Приятного прослушивания! ";
        this.podcastRepository.save(new Podcast("Психология: мифы и реальность.", podDesc, 221, 8, 0));
        podDesc = "Программа о том, как выучить английский язык быстро и без лишних усилий. Вы узнаете, как пополнить словарный запас, разобраться в правилах грамматики, поставить произношение. А самое главное — освоить логику английского языка и научиться думать на нём.";
        this.podcastRepository.save(new Podcast("Учим английский с Don’t Speak", podDesc, 159, 8, 0));
        podDesc = "Что может быть лучше душевной беседы? Только душевная беседа двух рыжих чуваков. Каждое воскресенье Константин Тростенюк и Денис Карамышев экспертно анализируют всё, что достойно экспертного анализа.";
        this.podcastRepository.save(new Podcast("Душевный подкаст", podDesc, 407, 13, 0));
        this.podcastRepository.save(new Podcast("Вечерние чтения", "Озвучиваю самое интересное из прочитанного за день.", 93, 2, 0));
        podDesc = "Радио ENERGY, частота в Москве 104.2 FM, получила официальное разрешение на использование всемирно известного товарного знака NRJ / Energy, которым владеет французская медиагруппа «Эн Эр Жи» («NRJ Group»). Это стало возможным в результате переговоров, проходивших в 2006 году между представителями Группы и «ВКПМ». NRJ создает специальные проекты, самым известным из которых является ежегодная музыкальная премия NRJ Music Awards. Радиостанции с торговой маркой NRJ / Energy работают в Австрии, Бельгии, Болгарии, Германии, Дании, Норвегии, Финляндии, Швейцарии, Швеции, а также в Ливане и на Украине.\n\nСетевое издание ENERGYFM.RU, свидетельство о регистрации СМИ Эл № ФС77-50193 от 15.06.2012, выдано Федеральной службой по надзору в сфере связи, информационных технологий и массовых коммуникаций (Роскомнадзор).";
        this.podcastRepository.save(new Podcast("NRJ RUSSIA", podDesc, 213, 5, 0));
        this.podcastRepository.save(new Podcast("Предназначение. Дело Жизни", "Обсуждаются такие вопросы:\nПредназначение\nМотивация\nЭмоциональное выгорание\nЛень и прокрастинация\nCамоопределение\nКак найти себяПрофориентация\nПризвание\nКем быть в жизни\nДело Жизни\nПродуктивность\n\nВ эфире:\nИстории раскрытия и монетизации дела жизни. Короткие практики и медитации для самостоятельной работы. Методики и модели раскрытия предназначения. Записи выступлений и вебинаров Павла Кочкина. Ответы на вопросы подписчиков.", 277, 8, 0));
        this.podcastRepository.save(new Podcast("Игровой Батискаф", "Лучшее со дна игровой индустрии в исполнении знатных водолазов.", 407, 13, 0));

        this.episodeRepository.save(new Episode(21, "Droider Cast 1", "1 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/50", new Date(2018, 9, 22)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 2", "2 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/51", new Date(2018, 10, 12)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 3", "3 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/52", new Date(2018, 10, 22)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 4", "4 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 10, 27)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 5", "5 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 11, 2)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 6", "6 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 11, 12)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 7", "7 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 11, 21)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 8", "8 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 12, 2)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 9", "9 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 12, 9)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 10", "10 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 12, 16)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 11", "11 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2018, 12, 24)));
        this.episodeRepository.save(new Episode(21, "Droider Cast 12", "12 выпуск подкаста \"Droider Cast\"", "https://droidercast.podster.fm/53", new Date(2019, 1, 5)));

        for (int i = 1; i <= 25; i++) {
            this.tweetRepository.save(new Tweet("Droider Cast Твит " + i + "!"));
        }

        this.coauthorsRepository.save(new CoauthorsEntity(21, 18));
        this.coauthorsRepository.save(new CoauthorsEntity(21, 19));
    }
}