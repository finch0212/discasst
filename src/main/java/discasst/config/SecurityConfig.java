package discasst.config;

import discasst.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static org.springframework.http.HttpMethod.POST;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserService userService;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(POST, "/getCoauthorsByIdList", "/getPodcastsInPage");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/podcast/**",
                        "/getCoauthors",
                        "/getPodcastById",
                        "/getEpisodesByPodcastIdInPage",
                        "/getEpisodesInFeedByUserIdInPage",
                        "/getPodcastsByUserIdInCoauthors",
                        "/user/**",
                        "/getAllGenres",
                        "/",
                        "/css/**",
                        "/generated/**",
                        "/images/**",
                        "/favicon.ico",
                        "/error",
                        "/register",
                        "/addUser",
                        "/getAllPodcasts",
                        "/getAllEpisodesByPodcastId",
                        "/getEpisodesCountToday",
                        "/getEpisodesCountThisMounth",
                        "/getPodcastsCountThisMounth",
                        "/getPodcastsCountToday",
                        "/getPodcastsByUserId/**",
                        "/getUsersCountThisMounth",
                        "/getUsersCountToday",
                        "/statistics",
                        "/getUserInfo",
                        "/getAllTwitsByPodcastId"

                ).permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/signin")
                .usernameParameter("username")
                .passwordParameter("password")
                .loginProcessingUrl("/perform_login")
                .defaultSuccessUrl("/", true)
                .failureUrl("/signin?error")
                .permitAll()
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).permitAll()
                .and().csrf()
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
}
