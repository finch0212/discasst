package discasst.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Data
@Entity
public class Tweet {
    @Id
    @GeneratedValue
    private long id;
    private long podcastId;
    private long authorId;
    private String authorUsername;
    private String text;
    private String creationDate;

    public Tweet(String text) {
        Random r = new Random();
        int v = r.nextInt(3);

        this.podcastId = 21;
        this.authorId = v == 0 ? 17 : v == 1 ? 18 : 19;
        this.text = text;
        this.creationDate = new SimpleDateFormat("dd.MM.yy HH:mm").format(new Date());
        this.authorUsername = v == 0 ? "vedensky" : v == 1 ? "istishev" : "ilyaliya";
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorNickName() {
        return authorUsername;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreationDate() {
        this.creationDate = new SimpleDateFormat("dd.MM.yy HH:mm").format(new Date());
    }
}
