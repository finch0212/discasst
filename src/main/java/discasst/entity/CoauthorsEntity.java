package discasst.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class CoauthorsEntity {
    @Id
    @GeneratedValue
    private long id;
    private long podcastId;
    private long userId;

    public CoauthorsEntity(long podcastId, long userId) {
        this.podcastId = podcastId;
        this.userId = userId;
    }
}
