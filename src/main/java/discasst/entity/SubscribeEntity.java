package discasst.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class SubscribeEntity {
    @Id
    @GeneratedValue
    private long id;
    private long userId;
    private long podcastId;
    private boolean isSubscribed;

    public SubscribeEntity(long userId, long podcastId, boolean sub) {
        this.userId = userId;
        this.podcastId = podcastId;
        this.isSubscribed = sub;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }
}
