package discasst.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Data
public class Episode {
    @Id
    @GeneratedValue
    private long id;
    private long podcastId;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String podsterLink;
    private String date;

    public long getPodcastId() {
        return podcastId;
    }

    public Episode(long podcastId, String name,
                   String description, String podsterLink, Date date) {
        this.podcastId = podcastId;
        this.name = name;
        this.description = description;
        this.podsterLink = podsterLink;
        this.date = new SimpleDateFormat("dd.MM.yy").format(date);
    }

    public void setDate(Date date) {
        this.date = new SimpleDateFormat("dd.MM.yy").format(date);
    }
}
