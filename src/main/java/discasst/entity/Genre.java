package discasst.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Genre {
    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String name;

    public Genre(String name) {
        this.name = name;
    }
}
