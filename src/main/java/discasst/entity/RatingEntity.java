package discasst.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class RatingEntity {
    @Id
    @GeneratedValue
    private long id;
    private long userId;
    private long podcastId;
    private boolean decision;

    public RatingEntity(long userId, long podcastId, boolean decision) {
        this.userId = userId;
        this.podcastId = podcastId;
        this.decision = decision;
    }

    public long getUserId() {
        return userId;
    }

    public long getPodcastId() {
        return podcastId;
    }

    public void setDecision(boolean decision) {
        this.decision = decision;
    }

    public boolean isDecision() {
        return decision;
    }
}
