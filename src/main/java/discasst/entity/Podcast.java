package discasst.entity;

import lombok.Data;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Data
@Table(name = "podcast")
public class Podcast {
    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true)
    private String name;
    private long authorId;
    @Column(columnDefinition="TEXT")
    private String description;
    private String itunesLink = "";
    private String vkLink = "";
    private String twitterLink = "";
    private String facebookLink = "";
    private Integer episodeCount = 0;
    private Integer subscribersCount = 0;
    private long genreId;
    private Integer rating = 0;
    private String creationDate;

    public Podcast(String name, String description, Integer rating, Integer genreId, int episodeCount) {
        this.name = name;
        this.description = description;
        this.rating = rating;
        this.genreId = genreId;
        this.episodeCount = episodeCount;
    }

    public Podcast(String name, String description, String itunesLink, String vkLink, String twitterLink,
                   String facebookLink, Integer episodeCount, long genreId, Integer rating, Integer subscribersCount, long authorId) {
        this.name = name;
        this.description = description;
        this.itunesLink = itunesLink;
        this.vkLink = vkLink;
        this.twitterLink = twitterLink;
        this.facebookLink = facebookLink;
        this.episodeCount = episodeCount;
        this.genreId = genreId;
        this.rating = rating;
        this.subscribersCount = subscribersCount;
        this.authorId = authorId;
    }

    public Integer getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(Integer episodeCount) {
        this.episodeCount = episodeCount;
    }

    public void setCreationDate(Date date) {
        this.creationDate = new SimpleDateFormat("dd.MM.yy").format(date);
    }

    public long getAuthorId() {
        return authorId;
    }

    public void ratingInc() {
        this.rating++;
    }

    public void ratingDec() {
        this.rating--;
    }

    public void subscribersCountDec() {
        this.subscribersCount--;
    }

    public void subscribersCountInc() {
        this.subscribersCount++;
    }
}
