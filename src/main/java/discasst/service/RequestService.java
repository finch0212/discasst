package discasst.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Класс для бинда первого входа на сайт (первая загрузка сайта не через реакт роутер)
 */
@Controller
public class RequestService {

    @RequestMapping(value = "/signin")
    public String signinPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/register")
    public String registerPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/search")
    public String searchPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/feed")
    public String feed() {
        return "MainHTML";
    }

    @RequestMapping(value = "/statistics")
    public String statisticsPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/")
    public String mainPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/user/{username}")
    public String userProfilePage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/podcast/{podcastId}")
    public String podcastPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/createNewPodcast")
    public String createNewPodcastPage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/createNewEpisode/{podcastId}")
    public String createNewEpisodePage() {
        return "MainHTML";
    }

    @RequestMapping(value = "/addCoauthors/{podcastId}")
    public String addCoauthorsPage() {
        return "MainHTML";
    }
}
