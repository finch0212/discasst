package discasst.service;

import com.google.gson.Gson;
import discasst.da.SubscribeRepository;
import discasst.da.TweetRepository;
import discasst.entity.Tweet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@Service
@RestController
public class TweetService {
    @Autowired
    private TweetRepository tweetRepository;
    @Autowired
    private SubscribeRepository subscribeRepository;

    @RequestMapping(value = "/addTweet", method = RequestMethod.POST, headers = {"Content-type=application/json"})
    public void create(@RequestBody String tweetJson) {
        Gson g = new Gson();
        Tweet newTwit = g.fromJson(tweetJson, Tweet.class);
        newTwit.setCreationDate();
        newTwit.setCreationDate();

        this.tweetRepository.save(newTwit);

        System.out.println("new tweet added:" + newTwit.getText());
    }

    @RequestMapping(value = "/deleteTweet", method = RequestMethod.POST)
    public void deleteById(@RequestParam(value = "tweetId") Long tweetId) {
        this.tweetRepository.deleteById(tweetId);
    }

    public Tweet getById(int id) {
        return null;
    }

    @RequestMapping(value = "/getAllTwitsByPodcastId")
    public Page<Tweet> getAllTwitsByUserId(@RequestParam(value = "id") Long id, Pageable pageable) {
        return tweetRepository.findAllByPodcastIdOrderByIdDesc(id, pageable);
    }

    @RequestMapping(value = "/getAllTwitsByUserIdFeed")
    public Page<Tweet> getAllTwitsByUserIdFeed(@RequestParam(value = "id") Long id, Pageable pageable) {
        long[] subs = subscribeRepository.findAllByUserId(id);
        return tweetRepository.findAllByPodcastIdInOrderByIdDesc(subs, pageable);
    }


}
