package discasst.service;

import com.google.gson.Gson;
import discasst.da.*;
import discasst.entity.CoauthorsEntity;
import discasst.entity.Episode;
import discasst.entity.Genre;
import discasst.entity.Podcast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@RestController
public class PodcastService {
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private PodcastRepository podcastRepository;
    @Autowired
    private EpisodeRepository episodeRepository;
    @Autowired
    private CoauthorsRepository coauthorsRepository;
    @Autowired
    private SubscribeRepository subscribeRepository;
    @Autowired
    private TweetRepository tweetRepository;
    @Autowired
    private RatingRepository ratingRepository;

    @RequestMapping(value = "/getGenres", method = RequestMethod.GET)
    public List<Genre> getGenres() {
        List<Genre> genres = genreRepository.findAll();
        if (genres != null) return genres;
        else throw new UsernameNotFoundException("Genres not found!");
    }

    @RequestMapping(value = "/createPodcast")
    public @ResponseBody
    void createPodcast(@RequestBody String podcast) {
        Gson g = new Gson();
        Podcast pod = g.fromJson(podcast, Podcast.class);
        pod.setCreationDate(new Date());
        podcastRepository.save(pod);
    }

    @Transactional
    @RequestMapping(value = "/removePodcast", method = RequestMethod.POST)
    public @ResponseBody
    void removePodcast(@RequestParam(value = "id") long id) {
        episodeRepository.deleteAllByPodcastId(id);
        coauthorsRepository.deleteAllByPodcastId(id);
        tweetRepository.deleteAllByPodcastId(id);
        subscribeRepository.deleteAllByPodcastId(id);
        ratingRepository.deleteAllByPodcastId(id);
        podcastRepository.deleteById(id);
    }

    @RequestMapping(value = "/getPodcastsByUserId")
    public @ResponseBody
    List<Podcast> getPodcastsByUserId(@RequestParam(value = "id") long id) {
        return podcastRepository.findAllByAuthorId(id);
    }

    @RequestMapping(value = "/getPodcastsByUserIdInCoauthors")
    public @ResponseBody
    List<Podcast> getPodcastsByUserIdInCoauthors(@RequestParam(value = "id") long id) {
        long[] ids = coauthorsRepository.findAllByUserId(id);
        return podcastRepository.findAllByIdIn(ids);
    }

    @RequestMapping(value = "/createEpisode")
    public @ResponseBody
    void createEpisode(@RequestBody String episode) {
        Gson g = new Gson();
        Episode newEpisode = g.fromJson(episode, Episode.class);
        newEpisode.setDate(new Date());
        episodeRepository.save(newEpisode);
        Podcast pod = podcastRepository.findById(newEpisode.getPodcastId());
        pod.setEpisodeCount(pod.getEpisodeCount() + 1);
        podcastRepository.save(pod);
    }

    @RequestMapping(value = "/getEpisodesByPodcastIdInPage")
    public @ResponseBody
    Page<Episode> getAllEpisodesByPodcastIdInPage(
            @RequestParam(value = "id") long id, Pageable pageable) {
        return episodeRepository.findAllByPodcastIdOrderByIdDesc(id, pageable);
    }

    @Transactional
    @RequestMapping(value = "/getEpisodesInFeedByUserIdInPage")
    public @ResponseBody
    Page<Episode> getEpisodesInFeedByUserIdInPage(@RequestParam(value = "id") long id, Pageable pageable) {
        long[] subs = subscribeRepository.findAllByUserId(id);
        return episodeRepository.findAllByPodcastIdInOrderByIdDesc(subs, pageable);
    }

    @RequestMapping(value = "/getEpisodesCountToday")
    public @ResponseBody
    int getEpisodesCountToday() throws ParseException {
        String today = new SimpleDateFormat("dd.MM.yy").format(new Date());
        return episodeRepository.findAllByDate(today).size();
    }

    @RequestMapping(value = "/getEpisodesCountThisMounth")
    public @ResponseBody
    int getEpisodesCountThisMounth() throws ParseException {
        String today = new SimpleDateFormat("MM.yy").format(new Date());
        return episodeRepository.findAllInThisMonth("%" + today + "%").size();
    }

    @RequestMapping(value = "/getPodcastsCountToday")
    public @ResponseBody
    int getPodcastsCountToday() throws ParseException {
        String today = new SimpleDateFormat("dd.MM.yy").format(new Date());
        return podcastRepository.findAllByCreationDate(today).size();
    }

    @RequestMapping(value = "/getPodcastsCountThisMounth")
    public @ResponseBody
    int getPodcastsCountThisMounth() throws ParseException {
        String today = new SimpleDateFormat("MM.yy").format(new Date());
        return podcastRepository.findAllInThisMonth("%" + today + "%").size();
    }

    @Transactional
    @RequestMapping(value = "/addCoauthors")
    public @ResponseBody
    void addCoauthors(@RequestBody String authorsIdsJSON, @RequestParam(value = "podcastId") long podcastId) {
        Gson g = new Gson();
        int[] authorsIds = g.fromJson(authorsIdsJSON, int[].class);
        coauthorsRepository.deleteAllByPodcastId(podcastId);
        for (int i : authorsIds) coauthorsRepository.save(new CoauthorsEntity(podcastId, i));
    }

    @RequestMapping(value = "/getCoauthors")
    public @ResponseBody
    List<CoauthorsEntity> getCoauthors(@RequestParam(value = "podcastId") long podcastId) {
        return coauthorsRepository.findAllByPodcastId(podcastId);
    }
}