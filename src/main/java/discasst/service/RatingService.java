package discasst.service;

import com.google.gson.Gson;
import discasst.da.PodcastRepository;
import discasst.da.RatingRepository;
import discasst.entity.Podcast;
import discasst.entity.RatingEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Service
@RestController
public class RatingService {
    @Autowired
    private PodcastRepository podcastRepository;
    @Autowired
    private RatingRepository ratingRepository;

    @Transactional
    @RequestMapping(value = "/vote", method = RequestMethod.POST)
    public @ResponseBody
    void vote(@RequestBody String rateStr) {
        Gson g = new Gson();
        RatingEntity newrate = g.fromJson(rateStr, RatingEntity.class);
        RatingEntity oldrate = ratingRepository.findByPodcastIdAndUserId(newrate.getPodcastId(), newrate.getUserId());
        Podcast podcast = podcastRepository.findById(newrate.getPodcastId());
        //если пользователь уже голосовал
        if (oldrate != null) {
            //если оценка отличается
            if (oldrate.isDecision() != newrate.isDecision()) {
                //если оценка положительная
                if (newrate.isDecision()) {
                    podcast.ratingInc();
                    podcast.ratingInc();
                    oldrate.setDecision(true);
                } else {
                    podcast.ratingDec();
                    podcast.ratingDec();
                    oldrate.setDecision(false);
                }
                ratingRepository.save(oldrate);
            }
        } else {
            ratingRepository.save(newrate);
            if (newrate.isDecision()) {
                podcast.ratingInc();
            } else {
                podcast.ratingDec();
            }
        }
        podcastRepository.save(podcast);
    }

    @RequestMapping(value = "/getMyVote", method = RequestMethod.GET)
    public @ResponseBody
    int getMyVote(@RequestParam(value = "userId") long userId, @RequestParam(value = "podcastId") long podcastId) {
        RatingEntity oldrate = ratingRepository.findByPodcastIdAndUserId(podcastId, userId);
        //если пользователь уже голосовал
        if (oldrate != null) {
            //если оценка положительная
            if (oldrate.isDecision()) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return 0;
        }
    }
}
