package discasst.service;

import discasst.da.PodcastRepository;
import discasst.da.SubscribeRepository;
import discasst.entity.Podcast;
import discasst.entity.SubscribeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@Service
@RestController
public class SubscribeService {
    @Autowired
    private SubscribeRepository subscribeRepository;
    @Autowired
    private PodcastRepository podcastRepository;

    @Transactional
    @RequestMapping(value = "/subscribe", method = RequestMethod.GET)
    public @ResponseBody
    void subscribe(@RequestParam(value = "userId") long userId, @RequestParam(value = "podcastId") long podcastId) {
        SubscribeEntity subs = subscribeRepository.findByPodcastIdAndUserId(podcastId, userId);
        Podcast podcast = podcastRepository.findById(podcastId);
        //если запись с таким пользователем и подкастом существует
        if (subs != null) {
            subs.setSubscribed(!subs.isSubscribed());
            subscribeRepository.save(subs);
            if (subs.isSubscribed()) {
                podcast.subscribersCountInc();
            } else {
                podcast.subscribersCountDec();
            }
        } else {
            subscribeRepository.save(new SubscribeEntity(userId, podcastId, true));
            podcast.subscribersCountInc();
        }
        podcastRepository.save(podcast);
    }

    @RequestMapping(value = "/getMySubscribe", method = RequestMethod.GET)
    public @ResponseBody
    boolean getMyVote(@RequestParam(value = "userId") long userId, @RequestParam(value = "podcastId") long podcastId) {
        SubscribeEntity subs = subscribeRepository.findByPodcastIdAndUserId(podcastId, userId);
        //если запись с таким пользователем и подкастом существует
        if (subs != null) {
            return subs.isSubscribed();
        } else {
            return false;
        }
    }
}
