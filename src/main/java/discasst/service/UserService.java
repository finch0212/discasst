package discasst.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import discasst.da.UserRepository;
import discasst.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@RestController
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);
        if (user != null) return user;
        else throw new UsernameNotFoundException("User " + s + " not found!");
    }

    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    public User getUserInfo(@RequestParam(value = "username", required = true) String username) {
        User user = userRepository.findByUsername(username);
        if (user != null) return user;
        else throw new UsernameNotFoundException("User " + username + " not found!");
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public void addUser(@RequestParam(value = "username", required = true) String username,
                        @RequestParam(value = "name", required = true) String name,
                        @RequestParam(value = "email", required = true) String email,
                        @RequestParam(value = "password", required = true) String password,
                        HttpServletRequest request, HttpServletResponse response) {
        User newUser = new User(username, email, new BCryptPasswordEncoder().encode(password), name);
        try {
            newUser.setCreationDate(new Date());
            userRepository.save(newUser);
            System.out.println("new user added: " + username);
            response.setHeader("newUserStatus", "ok");
        } catch (Exception e) {
            System.out.println("error on adding new user: " + e.getMessage());
            if (e.getCause().getCause().getMessage().contains("Нарушение уникального индекса или первичного ключа"))
                response.setHeader("newUserStatus", "usernameError");
            else response.setHeader("newUserStatus", "unknownError");
        }
    }

    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST)
    public void updateProfile(@RequestParam(value = "name", required = true) String name,
                              @RequestParam(value = "username", required = true) String username,
                              HttpServletRequest request, HttpServletResponse response) {
        try {
            User updatedUser = userRepository.findByUsername(username);
            updatedUser.setName(name);
            userRepository.save(updatedUser);
            System.out.println("profile updated: " + name);
            response.setHeader("updateProfileStatus", "ok");
        } catch (Exception e) {
            System.out.println("profile update error: " + e.getMessage());
            response.setHeader("updateProfileStatus", "error");
        }
    }

    @RequestMapping(value = "/getUsersCountToday")
    public @ResponseBody
    int getUsersCountToday() throws ParseException {
        String today = new SimpleDateFormat("dd.MM.yy").format(new Date());
        return userRepository.findAllByCreationDate(today).size();
    }

    @RequestMapping(value = "/getUsersCountThisMounth")
    public @ResponseBody
    int getUsersCountThisMounth() throws ParseException {
        String today = new SimpleDateFormat("MM.yy").format(new Date());
        return userRepository.findAllInThisMonth("%" + today + "%").size();
    }

    @RequestMapping(value = "/getCoauthorsByIdList", method = RequestMethod.POST)
    public @ResponseBody
    List<User> getCoauthorsByIdList(@RequestBody String coauthorsIdsJSON) {
        Gson g = new Gson();
        JsonElement json = new JsonPrimitive(coauthorsIdsJSON);
        long[] coauthorsIds = g.fromJson(coauthorsIdsJSON, Ids.class).ids;
        return userRepository.findAllByIdIn(coauthorsIds);
    }

    @RequestMapping(value = "/loginCheck", method = RequestMethod.POST)
    public @ResponseBody
    int loginCheck() {
        return 200;
    }

    public class Ids {
        long[] ids;
    }
}


