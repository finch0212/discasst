package discasst.service;

import com.google.gson.Gson;
import discasst.da.GenreRepository;
import discasst.da.PodcastRepository;
import discasst.entity.Genre;
import discasst.entity.Podcast;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@RestController
public class SearchService {
    @Autowired
    private PodcastRepository podcastRepository;
    @Autowired
    private GenreRepository genreRepository;

    @RequestMapping(value = "/getPodcastsInPage", method = RequestMethod.POST)
    public Page<Podcast> getPodcastsInPage(@RequestBody String json, Pageable pageable) {
        Gson g = new Gson();
        FilterAndGenres filterAndGenres = g.fromJson(json, FilterAndGenres.class);
        if (filterAndGenres.byRating) {
            return podcastRepository.findByNameContainsIgnoreCaseAndGenreIdNotInOrderByRatingDesc(filterAndGenres.filter, filterAndGenres.genres, pageable);
        } else {
            return podcastRepository.findByNameContainsIgnoreCaseAndGenreIdNotInOrderByIdDesc(filterAndGenres.filter, filterAndGenres.genres, pageable);
        }
    }

    @RequestMapping(value = "/getPodcastById", method = RequestMethod.GET)
    public Podcast getPodcastById(@RequestParam(value = "id", required = true) long id) {
        return podcastRepository.findById(id);
    }

    @RequestMapping(value = "/getAllGenres", method = RequestMethod.GET)
    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    class FilterAndGenres {
        public String filter = "";
        public long[] genres;
        public boolean byRating;
    }
}
